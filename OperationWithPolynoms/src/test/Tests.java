package test;

import element.*;
import operation.*;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.*;

public class Tests {
// P: x^5+2x^3+51x^4+120
//Q : x^2+100x^7-5x^5
	Polynom p = new Polynom();
	Polynom q = new Polynom();
	Calculator c;
	MonomInt mono;
	Polynom result;
	Polynom expected;

	public void setUp() {

		mono = new MonomInt();
		mono.setCoef(1);
		mono.setPower(5);
		p.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(2);
		mono.setPower(3);
		p.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(51);
		mono.setPower(4);
		p.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(120);
		mono.setPower(0);
		p.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(1);
		mono.setPower(2);
		q.setElement(mono);
	

		mono = new MonomInt();
		mono.setCoef(100);
		mono.setPower(7);
		q.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-5);
		mono.setPower(5);
		q.setElement(mono);

		p.sortElement();
		q.sortElement();

		c = new Calculator(p, q);
	}

	@Test
	public void testAdd() {
		setUp();
		result = new Polynom();
		result = c.addPolynoms();

		expected = new Polynom();
		mono = new MonomInt();
		mono.setCoef(-4);
		mono.setPower(5);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(2);
		mono.setPower(3);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(51);
		mono.setPower(4);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(120);
		mono.setPower(0);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(1);
		mono.setPower(2);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(100);
		mono.setPower(7);
		expected.setElement(mono);

		expected.sortElement();

		assertTrue(compare(expected, result));

	}

	@Test
	public void testSub() {
		setUp();
		result = new Polynom();
		result = c.substractPolynoms();
		expected = new Polynom();

		mono = new MonomInt();
		mono.setCoef(6);
		mono.setPower(5);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(2);
		mono.setPower(3);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(51);
		mono.setPower(4);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(120);
		mono.setPower(0);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-1);
		mono.setPower(2);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-100);
		mono.setPower(7);
		expected.setElement(mono);

		expected.sortElement();
		assertTrue(compare(expected, result));
	}
	@Test
	public void testMul() {
		setUp();
		result = new Polynom();
		result = c.multiplyPolynoms();
		
		
		expected = new Polynom();

		mono = new MonomInt();
		mono.setCoef(100);
		mono.setPower(12);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(5100);
		mono.setPower(11);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(195);
		mono.setPower(10);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-255);
		mono.setPower(9);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-10);
		mono.setPower(8);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(12001);
		mono.setPower(7);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(51);
		mono.setPower(6);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(-598);
		mono.setPower(5);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(120);
		mono.setPower(2);
		expected.setElement(mono);
		
		expected.sortElement();
		System.out.println("Inmultire");
	for(Monom m : expected.getElement())
		System.out.println(m);
	
	System.out.println("Inmultire");
	for(Monom m : result.getElement())
		System.out.println(m);
		
	
		
	assertTrue(compare(expected, result));

	}

	@Test
	public void testInt() {
		setUp();
		result = new Polynom();
		result = c.integratePolymon(p);
		MonomDoouble monom;

		expected = new Polynom();

		monom = new MonomDoouble();
		monom.setCoef((double) 1 / 6);
		monom.setPower(6);
		expected.setElement(monom);

		monom = new MonomDoouble();
		monom.setCoef((double) 2 / 4);
		monom.setPower(4);
		expected.setElement(monom);

		monom = new MonomDoouble();
		monom.setCoef((double) 51 / 5);
		monom.setPower(5);
		expected.setElement(monom);

		monom = new MonomDoouble();
		monom.setCoef(120);
		monom.setPower(1);
		expected.setElement(monom);
		expected.sortElement();
		assertTrue(compare(expected, result));

	}

	@Test
	public void testDiv() {
		setUp();
		result = new Polynom();
		c.dividePolynoms();
		expected = new Polynom();
		mono = new MonomInt();
		mono.setCoef(1);
		mono.setPower(5);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(2);
		mono.setPower(3);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(51);
		mono.setPower(4);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(120);
		mono.setPower(0);
		expected.setElement(mono);
		expected.sortElement();
		assertTrue(compare(expected, c.getRezidue()));
	}

	@Test
	public void testDervi() {
		setUp();
		result = new Polynom();
		result = c.derivatePolymon(p);

		expected = new Polynom();

		mono = new MonomInt();
		mono.setCoef(5);
		mono.setPower(4);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(6);
		mono.setPower(2);
		expected.setElement(mono);

		mono = new MonomInt();
		mono.setCoef(204);
		mono.setPower(3);
		expected.setElement(mono);

		expected.sortElement();

		System.out.println("Derivare");
		assertTrue(compare(expected, result));
	}

	boolean compare(Polynom result, Polynom compare) {
		int i;
		List<Monom> monomResult = result.getElement();
		List<Monom> monomCompare = compare.getElement();
		if (monomResult.size() != monomCompare.size())
			return false;
		for (i = 0; i < monomResult.size(); i++) {
			System.out.println(monomResult.get(i)+" " + monomCompare.get(i));
			if (!(monomResult.get(i).equals(monomCompare.get(i))))
				return false;
		}
		return true;

	}

}
