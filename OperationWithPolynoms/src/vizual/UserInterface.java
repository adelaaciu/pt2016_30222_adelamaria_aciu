package vizual;

import element.MonomInt;

import element.Polynom;
import operation.Calculator;

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class UserInterface is used to display in a user friendly way the operations
 * implemented for polinoms
 * 
 * @author Adela
 *
 */
public class UserInterface {

	private JFrame frame;
	private JTextField polymonPTextField;
	private JTextField polynomQTextField;
	private JTextArea textForAdd;
	private JTextArea textForSubb;
	private JTextArea textForMul;
	private JTextArea textForDiv;
	private JTextArea textForInteg;
	private JTextArea textForDeriv;
	private JTextArea textForDiv2;
	private Polynom p;
	private Polynom q;
	private Polynom rez;
	private Calculator c;
	private JTextArea textAreaForQ;
	private JTextArea textArea_1ForQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInterface window = new UserInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame and adding action listener for
	 * buttons and textArea
	 */
	private void initialize() {

		frame = new JFrame();
		frame.getContentPane().setForeground(SystemColor.activeCaption);
		frame.setForeground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 861, 559);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panelPolynoms = new JPanel();
		panelPolynoms.setForeground(SystemColor.activeCaption);
		panelPolynoms.setBounds(10, 11, 296, 258);
		frame.getContentPane().add(panelPolynoms);
		panelPolynoms.setLayout(null);

		JLabel lblPolynomP = new JLabel("Polynom P");
		lblPolynomP.setBounds(10, 27, 96, 14);
		panelPolynoms.add(lblPolynomP);

		polymonPTextField = new JTextField();
		polymonPTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				p = new Polynom();
				rez = new Polynom();
				searchMonom(polymonPTextField.getText());
				rez.eliminateSamePower(rez);
				Collections.sort(rez.getElement());
				p = rez;
			}
		});
		polymonPTextField.setBounds(10, 52, 276, 27);
		panelPolynoms.add(polymonPTextField);
		polymonPTextField.setColumns(10);

		JLabel lblPolynomQ = new JLabel("Polynom Q :");
		lblPolynomQ.setBounds(10, 148, 72, 14);
		panelPolynoms.add(lblPolynomQ);

		polynomQTextField = new JTextField();
		polynomQTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				q = new Polynom();
				rez = new Polynom();
				searchMonom(polynomQTextField.getText());
				rez.eliminateSamePower(rez);
				Collections.sort(rez.getElement());
				q = rez;
			}
		});
		polynomQTextField.setColumns(10);
		polynomQTextField.setBounds(10, 173, 276, 27);
		panelPolynoms.add(polynomQTextField);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(316, 11, 519, 498);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		/**
		 * Action listener for addButton used for add polinoms
		 */
		JButton addBtn = new JButton("+");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					if (q == null) {
						JOptionPane.showMessageDialog(null, "Introdu al doilea polinom");
					} else {
						turnOffEnable(p, q);
						c = new Calculator(p, q);
						Polynom result;
						result = c.addPolynoms();
						if (c.isZero(result))
							textForAdd.setText("0");
						else {
							Collections.sort(result.getElement());
							Collections.reverse(result.getElement());
							textForAdd.setText(c.createPolynomForInterface(result));
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "Introdu cele doua polinoame");

			}

		});
		addBtn.setBounds(10, 11, 41, 32);
		panel_1.add(addBtn);

		/**
		 * Action listener for substractButton used for substract polinoms
		 */
		JButton substractBtn = new JButton("-");
		substractBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					if (q == null) {
						JOptionPane.showMessageDialog(null, "Introdu al doilea polinom");
					} else {
						turnOffEnable(p, q);
						c = new Calculator(p, q);
						Polynom result;
						result = c.substractPolynoms();
						if (c.isZero(result))
							textForSubb.setText("0");
						else {
							Collections.sort(result.getElement());
							Collections.reverse(result.getElement());
							textForSubb.setText(c.createPolynomForInterface(result));
						}

					}
				} else
					JOptionPane.showMessageDialog(null, "Introdu cele doua polinoame");

			}
		});
		substractBtn.setBounds(10, 72, 41, 32);
		panel_1.add(substractBtn);

		/**
		 * Action listener for multiply Button used for multiplying polinoms
		 */
		JButton multiplyBtn = new JButton("*");
		multiplyBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					if (q == null) {
						JOptionPane.showMessageDialog(null, "Introdu al doilea polinom");
					} else {
						turnOffEnable(p, q);
						c = new Calculator(p, q);

						Polynom result;
						result = c.multiplyPolynoms();
						if (c.isZero(result))
							textForMul.setText("0");
						else {
							Collections.sort(result.getElement());
							Collections.reverse(result.getElement());

							textForMul.setText(c.createPolynomForInterface(result));
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "Introdu cele doua polinoame");

			}
		});
		multiplyBtn.setBounds(10, 130, 41, 32);
		panel_1.add(multiplyBtn);

		/**
		 * Action listener for divide Button used for dividing polinoms
		 */
		JButton divideBtn = new JButton("/");
		divideBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					if (q == null) {
						JOptionPane.showMessageDialog(null, "Introdu al doilea polinom");
					} else {
						turnOffEnable(p, q);
						c = new Calculator(p, q);
						c.dividePolynoms();
						if (c.getDiv() == false) {
							Polynom cat = new Polynom();
							cat = c.getDividedPolinom();
							Polynom rest = new Polynom();
							rest = c.getRezidue();

							Collections.sort(cat.getElement());
							Collections.reverse(cat.getElement());
							Collections.sort(rest.getElement());
							Collections.reverse(rest.getElement());

							textForDiv.setText(c.createPolynomForInterface(cat));
							textForDiv2.setText(c.createPolynomForInterface(rest));
						} else {
							textForDiv.setText("0");
							textForDiv2.setText("0");
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "Introdu cele doua polinoame");
			}
		});
		divideBtn.setBounds(10, 192, 41, 32);
		panel_1.add(divideBtn);

		/**
		 * Action listener for integrate Button used for integrating polinom P
		 */

		JButton integrateBtn = new JButton("~P");
		integrateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					c = new Calculator(p);
					polymonPTextField.setEditable(false);
					Polynom result;
					result = c.integratePolymon(p);
					if (c.isZero(result))
						textForInteg.setText("0");
					else {
						Collections.sort(result.getElement());
						Collections.reverse(result.getElement());

						textForInteg.setText(c.createPolynomForInterface(result));
					}
				} else
					JOptionPane.showMessageDialog(null, "Introdu polinomul P");

			}
		});
		integrateBtn.setBounds(0, 258, 51, 43);
		panel_1.add(integrateBtn);

		/**
		 * Action listener for derived Button used for deriving polinom P
		 */
		JButton DerivateBtn = new JButton("'P");
		DerivateBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				if (p != null) {
					polymonPTextField.setEditable(false);
					c = new Calculator(p);

					Polynom result;
					result = c.derivatePolymon(p);

					if (c.isZero(result))
						textForDeriv.setText("0");
					else {
						Collections.sort(result.getElement());
						Collections.reverse(result.getElement());

						textForDeriv.setText(c.createPolynomForInterface(result));
					}

				} else {
					JOptionPane.showMessageDialog(null, "Introdu polinomul P");
				}

			}
		});
		DerivateBtn.setBounds(0, 312, 51, 43);
		panel_1.add(DerivateBtn);

		textForAdd = new JTextArea();
		textForAdd.setEditable(false);
		textForAdd.setBounds(62, 11, 447, 32);
		panel_1.add(textForAdd);

		textForSubb = new JTextArea();
		textForSubb.setEditable(false);
		textForSubb.setBounds(61, 72, 448, 32);
		panel_1.add(textForSubb);

		textForMul = new JTextArea();
		textForMul.setEditable(false);
		textForMul.setBounds(61, 130, 448, 32);
		panel_1.add(textForMul);

		textForDiv = new JTextArea();
		textForDiv.setEditable(false);
		textForDiv.setBounds(62, 176, 447, 32);
		panel_1.add(textForDiv);

		textForInteg = new JTextArea();
		textForInteg.setEditable(false);
		textForInteg.setBounds(62, 258, 447, 32);
		panel_1.add(textForInteg);

		textForDeriv = new JTextArea();
		textForDeriv.setEditable(false);
		textForDeriv.setBounds(61, 323, 448, 32);
		panel_1.add(textForDeriv);

		textForDiv2 = new JTextArea();
		textForDiv2.setEditable(false);
		textForDiv2.setBounds(62, 220, 447, 27);

		panel_1.add(textForDiv2);
		/**
		 * Action listener for integrate Button used for integrating polinom Q
		 */

		JButton IntegrareQ = new JButton("~Q");
		IntegrareQ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (q != null) {
					polynomQTextField.setEditable(false);
					c = new Calculator(q);
					Polynom result;
					result = c.integratePolymon(q);
					if (c.isZero(result))
						textAreaForQ.setText("0");
					else {
						Collections.sort(result.getElement());
						Collections.reverse(result.getElement());
						textAreaForQ.setText(c.createPolynomForInterface(result));
					}
				} else {
					JOptionPane.showMessageDialog(null, "Introdu polinomul Q");
				}
			}
		});
		IntegrareQ.setBounds(0, 375, 51, 43);
		panel_1.add(IntegrareQ);

		/**
		 * Action listener for derived Button used for deriving polinom Q
		 */

		JButton DerivareQ = new JButton("'Q");
		DerivareQ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (q != null) {
					c = new Calculator(q);
					polynomQTextField.setEditable(false);
					Polynom result;
					result = c.derivatePolymon(q);
					if (c.isZero(result))
						textArea_1ForQ.setText("0");
					else {
						Collections.sort(result.getElement());
						Collections.reverse(result.getElement());
						textArea_1ForQ.setText(c.createPolynomForInterface(result));
					}
				} else {
					JOptionPane.showMessageDialog(null, "Introdu polinomul Q");
				}
			}
		});
		DerivareQ.setBounds(0, 426, 51, 43);
		panel_1.add(DerivareQ);

		textAreaForQ = new JTextArea();
		textAreaForQ.setEditable(false);
		textAreaForQ.setBounds(62, 375, 447, 32);
		panel_1.add(textAreaForQ);

		textArea_1ForQ = new JTextArea();
		textArea_1ForQ.setEditable(false);
		textArea_1ForQ.setBounds(61, 440, 448, 32);
		panel_1.add(textArea_1ForQ);

		JLabel lblR = new JLabel("R:");
		lblR.setBounds(30, 233, 21, 14);
		panel_1.add(lblR);

		JLabel lblC = new JLabel("C:");
		lblC.setBounds(30, 173, 21, 14);
		panel_1.add(lblC);

		JButton btnResetValues = new JButton("Reset values");
		btnResetValues.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				polymonPTextField.setText(null);
				polynomQTextField.setText(null);
				textForAdd.setText("");
				textForDeriv.setText("");
				textForDiv.setText("");
				textForDiv2.setText("");
				textForInteg.setText("");
				textForMul.setText("");
				textForSubb.setText("");
				textAreaForQ.setText(null);
				textArea_1ForQ.setText(null);
				polymonPTextField.setEditable(true);
				polynomQTextField.setEditable(true);
				p = null;
				q = null;
			}
		});
		btnResetValues.setBounds(101, 339, 113, 23);
		frame.getContentPane().add(btnResetValues);
		
		JLabel lblUnPolinomEste = new JLabel("Un polinom este de forma: 3x^4+5x^2-1");
		lblUnPolinomEste.setBounds(47, 283, 234, 23);
		frame.getContentPane().add(lblUnPolinomEste);
	}

	/**
	 * Method searchMonom search in a string which are the monoms
	 * 
	 * @param polinom
	 *            is the string in which you search the monom
	 */
	public void searchMonom(String polinom) {

		int i;
		int j = 0;
		String monom;
		if (polinom != null) {
			for (i = 1; i < polinom.length(); i++) {
				if ((polinom.charAt(i) == '+' || polinom.charAt(i) == '-')) {
					monom = polinom.substring(j, i);
					if (polinom.charAt(i) == '-')
						j = i;
					else if (polinom.charAt(i) == '+')
						j = i + 1;

					System.out.println(monom);
					obtainValuesForMonom(monom);
				}

			}

			monom = polinom.substring(j, polinom.length());
			System.out.println(monom);
			obtainValuesForMonom(monom);
		}

	}

	/**
	 * Method obtainValuesForMonom verify if the syntax for monom is correct and
	 * store it in a list
	 * 
	 * @param monom
	 *            is the input string from which I get the coefficient and power
	 *            for the monom
	 */
	public void obtainValuesForMonom(String monom) {
		String coef = "";
		String power = "";
		Integer momomPower = 0;
		Integer monomCoef = 0;
		MonomInt m = new MonomInt();
		int i = 0;

		try {

			System.out.println(monom.length());
			if (monom.charAt(monom.length() - 1) == 'x') {
				if (monom.charAt(0) != 'x')
					if (monom.charAt(0) == '-') {
						m.setCoef(-1);
						m.setPower(1);
					} else {
						coef = monom.substring(0, monom.length() - 1);
						monomCoef = Integer.parseInt(coef);
						m.setCoef(monomCoef);
						m.setPower(1);
					}
				else {

					m.setCoef(1);
					m.setPower(1);
				}

			} else {
				i = 0;

				while (monom.charAt(i) != 'x' && i < monom.length() - 1) {
					i++;
				}

				if (i != monom.length() - 1) {

					if (monom.charAt(0) == 'x') {
						m.setCoef(1);
					} else if (monom.charAt(1) == 'x' && monom.charAt(0) == '-') {
						m.setCoef(-1);
					} else {
						coef = monom.substring(0, i);
						System.out.println(coef);
						monomCoef = Integer.parseInt(coef);

						m.setCoef(monomCoef);

					}
					if (monom.charAt(i + 1) == '^') {

						power = monom.substring(i + 2, monom.length());
						momomPower = Integer.parseInt(power);
						m.setPower(momomPower);
					} else {

						m.setPower(1);
					}
				} else {

					monomCoef = Integer.parseInt(monom);
					m.setPower(0);
					m.setCoef(monomCoef);

				}
			}
			if (m != null) {
				System.out.println("Monom pow " + m.getPower() + " coef " + m.getCoef());
				rez.setElement(m);
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Bad imput. Corect your mistake and try again");
		}
	}

	/**
	 * Method turnOffEnable is used to set Enable to false for textFields
	 * 
	 * @param p1
	 *            First polinom
	 * @param p2
	 *            Second polinom
	 */
	public void turnOffEnable(Polynom p1, Polynom p2) {
		if (p1 != null && p2 != null) {
			polymonPTextField.setEditable(false);
			polynomQTextField.setEditable(false);
		}
	}
}
