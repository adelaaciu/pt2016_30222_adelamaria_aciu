package operation;

import java.util.*;

import javax.swing.JOptionPane;

import element.*;

/**
 * Class Calculator is used to compute operation for polimons
 * 
 * @author Adela
 *
 */
public class Calculator {
	/**
	 * @param firstPolynomP
	 *            first polinom for operations
	 * @param secondPolynomQ
	 *            second polinom for operations
	 * @param noDivide
	 *            tells if you can divide the two polinoms
	 * @param resultAdd
	 *            the result obtained from adding two polinoms
	 * @param resultSub
	 *            the result obtained from subtracting two polinoms
	 * @param resultMul
	 *            the result obtained from multiplying two polinoms
	 * @param resultIntegrate
	 * @param resultDerivate
	 * @param resultDivision
	 *            the result obtained from dividing two polinoms - cat
	 * @param resultRezidue
	 *            the result obtained from dividing two polinoms - rest
	 * @param pList
	 *            list of monom from first polimon
	 * @param qList
	 *            list of monom from first polimon
	 */
	private Polynom firstPolynomP;
	private Polynom secondPolynomQ;
	private boolean noDivide;
	private Polynom resultAdd;
	private Polynom resultSub;
	private Polynom resultMul;
	private Polynom resultIntegrate;
	private Polynom resultDerivate;
	private Polynom resultDivision;
	private Polynom resultRezidue;
	private List<Monom> pList;
	private List<Monom> qList;

	/**
	 * 
	 * @param p
	 *            first polinom
	 * @param q
	 *            second polinom
	 * @param pList
	 *            list of monom from first polimon
	 * @param qList
	 *            list of monom from first polimon
	 */
	public Calculator(Polynom p, Polynom q) {

		firstPolynomP = p;
		secondPolynomQ = q;
		pList = p.getElement();
		qList = q.getElement();
	}

	/**
	 * 
	 * @param p
	 *            a polinom
	 * @param pList
	 *            list of monom of p
	 * 
	 */
	public Calculator(Polynom p) {
		firstPolynomP = p;
		pList = p.getElement();
	}

	/**
	 * Method addPolynoms is used to add two polinoms using their monom list
	 * 
	 * @return resultAdd : the polinom obtained by adding two polinoms
	 */
	public Polynom addPolynoms() {
		int i = 0;
		int j = 0;
		int sum = 0;
		resultAdd = new Polynom();
		while (i < pList.size() && j < qList.size()) {

			// if both monoms have the same power

			if (pList.get(i).getPower() == qList.get(j).getPower()) {

				MonomInt monom = new MonomInt();
				MonomInt monomP = (MonomInt) pList.get(i);
				MonomInt monomQ = (MonomInt) qList.get(j);
				sum = monomP.getCoef() + monomQ.getCoef();

				monom.setCoef(sum);
				monom.setPower(pList.get(i).getPower());

				resultAdd.setElement(monom);
				i++;
				j++;
			}

			else if (pList.get(i).getPower() < qList.get(j).getPower()) {

				resultAdd.setElement(pList.get(i));
				i++;
			} else {
				resultAdd.setElement(qList.get(j));
				j++;
			}

		}
		while (j < qList.size()) {
			resultAdd.setElement(qList.get(j));
			j++;
		}

		while (i < pList.size()) {
			resultAdd.setElement(pList.get(i));
			i++;
		}
		resultAdd.setMonomList(resultAdd.deleteZeroCoef(resultAdd));
		resultAdd.sortElement();
		return resultAdd;

	}

	/**
	 * Method substractPolynoms is used to substract two polinoms using their
	 * monom list
	 * 
	 * @return resultSubb : the polinom obtained by substracting polinoms
	 */

	public Polynom substractPolynoms() {
		int i = 0;
		int j = 0;

		resultSub = new Polynom();
		while (i < pList.size() && j < qList.size()) {

			if (pList.get(i).getPower() == qList.get(j).getPower()) {

				if (pList.get(i) instanceof MonomInt && qList.get(j) instanceof MonomInt) {
					int substract = 0;
					MonomInt monom = new MonomInt();
					MonomInt monomP = (MonomInt) pList.get(i);
					MonomInt monomQ = (MonomInt) qList.get(j);
					substract = monomP.getCoef() - monomQ.getCoef();

					monom.setCoef(substract);
					monom.setPower(pList.get(i).getPower());

					resultSub.setElement(monom);
				} else {

					double substract = 0.0;
					MonomDoouble monom = new MonomDoouble();

					if (pList.get(i) instanceof MonomDoouble && qList.get(j) instanceof MonomDoouble) {
						substract = ((MonomDoouble) pList.get(i)).getCoef() - ((MonomDoouble) qList.get(j)).getCoef();
					} else {
						if (pList.get(i) instanceof MonomInt && qList.get(j) instanceof MonomDoouble) {
							substract = (double) ((MonomInt) pList.get(i)).getCoef()
									- ((MonomDoouble) qList.get(j)).getCoef();

						} else {
							substract = ((MonomDoouble) pList.get(i)).getCoef()
									- (double) ((MonomInt) qList.get(j)).getCoef();
						}
					}
					monom.setCoef(substract);
					monom.setPower(pList.get(i).getPower());
					resultSub.setElement(monom);

				}

				i++;
				j++;
			}

			else if (pList.get(i).getPower() < qList.get(j).getPower()) {

				resultSub.setElement(pList.get(i));
				i++;
			} else {
				if (qList.get(j) instanceof MonomInt) {
					int substract = 0;
					int coef = 0;
					MonomInt monom2 = new MonomInt();
					coef = ((MonomInt) qList.get(j)).getCoef();
					substract = 0 - coef;
					monom2.setCoef(substract);
					monom2.setPower(qList.get(j).getPower());
					resultSub.setElement(monom2);
				} else {

					double substract = 0.0;
					double coef = 0.0;
					MonomDoouble monom2 = new MonomDoouble();
					coef = ((MonomDoouble) qList.get(j)).getCoef();
					substract = 0.0 - coef;
					monom2.setCoef(substract);
					monom2.setPower(qList.get(j).getPower());
					resultSub.setElement(monom2);

				}

				j++;
			}

		}
		while (j < qList.size())

		{

			int coef;
			double coefDouble;
			if (qList.get(j) instanceof MonomInt) {
				MonomInt m = new MonomInt();

				coef = 0 - ((MonomInt) qList.get(j)).getCoef();
				System.out.println(coef);
				m.setCoef(coef);
				m.setPower(qList.get(j).getPower());
				resultSub.setElement(m);

			} else {

				MonomDoouble m = new MonomDoouble();
				coefDouble = 0.0 - ((MonomDoouble) qList.get(j)).getCoef();
				m.setCoef(coefDouble);
				m.setPower(qList.get(j).getPower());
				resultSub.setElement(m);

			}

			j++;
		}

		while (i < pList.size())

		{
			resultSub.setElement(pList.get(i));
			i++;
		}
		resultSub.setMonomList(resultSub.deleteZeroCoef(resultSub));
		resultSub.sortElement();
		return resultSub;

	}

	/**
	 * Method multiplyPolynoms is used to multiply two polinoms using their
	 * monom list
	 * 
	 * @return resultMul : the polinom obtained by multiplying polinoms
	 */

	public Polynom multiplyPolynoms() {
		int i = 0;
		int j = 0;
		int newPower = 0;

		resultMul = new Polynom();

		for (i = 0; i < pList.size(); i++)
			for (j = 0; j < qList.size(); j++) {

				newPower = pList.get(i).getPower() + qList.get(j).getPower();

				if (pList.get(i) instanceof MonomInt) {
					int newCof = 0;
					MonomInt mono = new MonomInt();
					newCof = ((MonomInt) pList.get(i)).getCoef() * ((MonomInt) qList.get(j)).getCoef();
					mono.setCoef(newCof);
					mono.setPower(newPower);
					resultMul.setElement(mono);
				} else {
					double newCof = 0.0;
					MonomDoouble mono = new MonomDoouble();
					newCof = ((MonomDoouble) pList.get(i)).getCoef() * ((MonomInt) qList.get(j)).getCoef();
					mono.setCoef(newCof);
					mono.setPower(newPower);
					resultMul.setElement(mono);

				}
			}

		resultMul.eliminateSamePower(resultMul);
		resultMul.sortElement();
		return resultMul;

	}

	public int getRank(Polynom polynom) {
		int rank = 0;
		List<Monom> polynomList = polynom.getElement();

		rank = polynomList.get(polynomList.size() - 1).getPower();

		return rank;
	}

	/**
	 * 
	 * @param monom
	 *            is a list of monoms
	 * @return true if the list is empty or all the coefficients are zero
	 */
	public boolean isEnd(List<Monom> monom) {
		boolean end = true;

		MonomDoouble md;
		MonomInt mi;
		for (Monom m : monom) {
			try {
				md = (MonomDoouble) m;

				if (md.getCoef() != 0.0)
					end = false;
			} catch (Exception e) {
				mi = (MonomInt) m;
				if (mi.getCoef() != 0)
					end = false;
			}
		}

		return end;

	}

	/**
	 * 
	 * @param pol
	 *            polinom you want to tests if it's zero
	 * @return true if the condition is respected
	 */

	public boolean isZero(Polynom pol) {

		for (Monom m : pol.getElement())
			if (m instanceof MonomInt) {
				if (((MonomInt) m).getCoef() != 0)
					return false;
			} else if (((MonomDoouble) m).getCoef() != 0.0)
				return false;

		return true;
	}

	/**
	 * Method dividePolynoms is used to divide two polinoms using their monom
	 * list
	 * 
	 * @param substituteForP
	 *            a substitute for first polinom
	 * @param substituteForQ
	 *            a substitute for second polinom
	 * @param finalPol
	 *            is the residue
	 * @param monomP
	 *            the list of monom for first polinom
	 * @param monomQ
	 *            the list of monom for second polinom
	 * @param newDiv
	 *            is the result obtained by multiplying second polinom with the
	 *            "cat" (nu am stiut cum se zice la cat in engleza)
	 * @param newRez
	 *            represent the "cat" (nu am stiut cum se zice la cat in
	 *            engleza)
	 * @param forRez
	 *            is the monom obtained by dividing the biggest coefficient of
	 *            the new first polinom and second polinom
	 * @return resultMul : the polinom obtained by divideing polinoms
	 */

	public void dividePolynoms() {

		Polynom substituteForP = firstPolynomP;
		Polynom substituteForQ = secondPolynomQ;
		List<Monom> monomP = substituteForP.getElement();
		List<Monom> monomQ = substituteForQ.getElement();

		Polynom newDiv = new Polynom();
		Polynom newRez = new Polynom();
		MonomDoouble forRez = new MonomDoouble();
		Polynom finalPol = new Polynom();
		finalPol = firstPolynomP;
		double coef = 0.0;
		int power;
		int i, j;

		if (isZero(secondPolynomQ)) {
			JOptionPane.showMessageDialog(null, "Nu se poate face impartirea cu 0");
			noDivide = true;

		} else

		if (getRank(substituteForP) < getRank(substituteForQ)) {
			resultRezidue = substituteForP;
			resultDivision = new Polynom();
			MonomInt m = new MonomInt();
			m.setCoef(0);
			m.setPower(0);
			resultDivision.setElement(m);
		} else {
			System.out.println("Intra in ramura else");

			while (getRank(finalPol) >= getRank(secondPolynomQ)) {

				i = monomP.size() - 1;
				j = monomQ.size() - 1;

				// calculate new coefficient
				if (monomP.get(i) instanceof MonomInt && monomQ.get(j) instanceof MonomInt) {
					coef = (double) ((MonomInt) monomP.get(i)).getCoef() / ((MonomInt) monomQ.get(j)).getCoef();
				} else if (monomP.get(i) instanceof MonomDoouble && monomQ.get(j) instanceof MonomInt) {
					coef = (double) ((MonomDoouble) monomP.get(i)).getCoef() / ((MonomInt) monomQ.get(j)).getCoef();
				} else if (monomP.get(i) instanceof MonomInt && monomQ.get(j) instanceof MonomDoouble) {
					coef = (double) ((MonomInt) monomP.get(i)).getCoef() / ((MonomDoouble) monomQ.get(j)).getCoef();
				}

				// generate new power
				power = monomP.get(i).getPower() - monomQ.get(j).getPower();

				// create new monom
				forRez = new MonomDoouble();
				forRez.setCoef(coef);
				forRez.setPower(power);

				// add to the list the monom
				newRez.setElement(forRez);

				// reply the first polinom
				firstPolynomP = newRez;
				pList = newRez.getElement();

				// multiply the new first polinom with the second one
				newDiv = multiplyPolynoms();

				// first polinom has it's first value
				firstPolynomP = substituteForP;

				// second polinom is the result obtained in multiplying function
				secondPolynomQ = newDiv;

				pList = firstPolynomP.getElement();
				qList = secondPolynomQ.getElement();

				// the residue obtaind by subtracting first and second polinom
				finalPol = substractPolynoms();

				if (!isEnd(finalPol.getElement())) {
					monomP = finalPol.deleteZeroCoef(finalPol);
					finalPol.setMonomList(monomP);
				} else
					monomP = finalPol.getElement();

				// first and second polinom return to their initial values
				firstPolynomP = substituteForP;
				secondPolynomQ = substituteForQ;

				pList = firstPolynomP.getElement();
				qList = secondPolynomQ.getElement();

				// restults are kept in resultDivision and resultRezidue
				resultDivision = newRez;
				resultRezidue = finalPol;

				if (isEnd(monomP)) {
					System.out.println("break");
					break;
				}
			}

		}

	}

	/**
	 * Method integratePolymon is used to integrate two polinoms using their
	 * monom list
	 * 
	 * @param pol
	 *            the polinom you want to integrate
	 * @param newCoef
	 *            the coefficient obtained after integrate
	 * @param newPower
	 *            the power obtained after integrate
	 * @return resultIntegrate :the result after integrating
	 */

	public Polynom integratePolymon(Polynom pol) {
		double newCoef;
		int newPower;
		resultIntegrate = new Polynom();
		MonomDoouble mono;

		for (Monom m : pol.getElement()) {
			newPower = m.getPower() + 1;
			newCoef = ((MonomInt) m).getCoef() / (double) newPower;
			mono = new MonomDoouble();
			mono.setCoef(newCoef);
			mono.setPower(newPower);

			resultIntegrate.setElement(mono);

		}

		return resultIntegrate;

	}

	/**
	 * Method derivatePolymon is used to derive two polinoms using their monom
	 * list
	 * 
	 * @param pol
	 *            the polinom you want to derive
	 * @param newCoef
	 *            the coefficient obtained after derive
	 * @param newPower
	 *            the power obtained after derive
	 * @return resultDerivate :the result after derive
	 */

	public Polynom derivatePolymon(Polynom pol) {
		int newCoef;
		int newPower;
		resultDerivate = new Polynom();
		MonomInt mono;

		for (Monom m : pol.getElement()) {
			if (m.getPower() != 0) {
				newCoef = ((MonomInt) m).getCoef() * m.getPower();
				newPower = m.getPower() - 1;
				mono = new MonomInt();
				mono.setCoef(newCoef);
				mono.setPower(newPower);
			} else {
				mono = new MonomInt();
				mono.setCoef(0);
				mono.setPower(0);
			}
			resultDerivate.setElement(mono);

		}
		resultDerivate.setMonomList(resultDerivate.deleteZeroCoef(resultDerivate));
		return resultDerivate;
	}

	/**
	 * Method createPolynomForInterface is used to create a display of a polinom
	 * 
	 * @param result
	 *            the polinom you want to display
	 * @return a string that represent the polinom
	 */
	public String createPolynomForInterface(Polynom result) {

		String polynom = new String();
		for (Monom m : result.getElement()) {
			polynom = polynom.concat(m.toString());
			System.out.println(m);
		}

		return polynom;

	}

	public Polynom getDividedPolinom() {

		return resultDivision;

	}

	public Polynom getRezidue() {
		return resultRezidue;
	}

	public boolean getDiv() {
		return noDivide;
	}
}
