package element;

/**
 * 
 * Class MonomInt represents a monom with a integer coefficient and integer
 * power
 *
 */

public class MonomInt extends Monom {
	/**
	 * @param coef: the coefficient of monom 
	 * @param power : power of monom
	 */
	
	private int coef;
	private int power;

	/**
	 * Method setPower sets the power of the monom
	 */
	
	public void setPower(int power) {
		this.power = power;
	}

	/**
	 * Method getPower returns the power of the monom
	 */
	
	public int getPower() {

		return this.power;
	}

	/**
	 * 
	 * @param coef set the coefficient from monom
	 */
	public void setCoef(int coef) {
		this.coef = coef;
	}

	/**
	 * Method getCoef returns the coefficient of the monom
	 */
	public int getCoef() {

		return this.coef;
	}

	
	/**
	 * Method toString returns a certain view of the monom
	 */
	@Override
	public String toString() {

		if (this.power == 1) {
			if (this.coef == 1)
				return "+X";
			else if (coef < 0) {
				if (coef != -1)
					return this.coef + "X";
				else
					return "-X";
			} else
				return "+" + this.coef + "X";
		} else 
			if (this.power == 0) {
			if (coef < 0) {
				return this.coef + "";
			} else
				return "+" + this.coef + "";
		} else {
			if (this.coef == 1)
				return "X^" + this.power;
			else if (coef < 0) {
				if (coef != -1)
					return this.coef + "X^" + this.power;
				else
					return "-X^" + this.power;
			} else
				return "+" + this.coef + "X^" + this.power;

		}
	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coef;
		result = prime * result + power;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonomInt other = (MonomInt) obj;
		if (coef != other.coef)
			return false;
		if (power != other.power)
			return false;
		return true;
	}
	
	
}
