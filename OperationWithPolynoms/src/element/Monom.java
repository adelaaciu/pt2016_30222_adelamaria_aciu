package element;
/**
 * 
 * Class Monom represent a general monom with integer power and implemets comparable to sort monoms from one polynom list
 *
 */
public abstract class Monom implements Comparable<Monom> {

	public abstract void setPower(int power);

	public abstract int getPower();
/**
 *  compareTo is used to compare two monoms by power
 */
	public int compareTo(Monom obj) {
		if (obj != null && this != null) {
			int pow1, pow2;
			pow2 = obj.getPower();
			pow1 = this.getPower();
			if (pow1 > pow2)
				return 1;
			if (pow1 < pow2)
				return -1;
		}
		return 0;

	}
}
