package element;

import java.util.*;

/**
 * 
 * class Polynom represent the input polinoms
 *
 */
public class Polynom {

	/**
	 * monomElement is a list of monoms that represents the parts of the polinom
	 */
	private List<Monom> monomElement;

	/**
	 * Constructor Polynom is used to initialize the arraylist
	 */
	public Polynom() {

		monomElement = new ArrayList<>();

	}

	/**
	 * Method eliminateSamePower verify if there are more monoms that have the
	 * same power. If it's true, a new coefficient is made by summing all the
	 * coefficients of the monoms, and removes the ones that are not nedded
	 * 
	 * @param result
	 *            represent the polinom from which we want to remove monoms with
	 *            the same power
	 * @param mono
	 *            the list of monoms from polinom
	 * 
	 */
	public void eliminateSamePower(Polynom result) {
		int i;
		int j = 1;
		List<Monom> mono = result.getElement();
		int newCoef = 0;
		double coef;
		for (i = 0; i < mono.size(); i++) {
			j = i + 1;
			while (j < mono.size()) {

				if (mono.get(i).getPower() == mono.get(j).getPower()) {
					if (mono.get(i) instanceof MonomInt && mono.get(j) instanceof MonomInt) {
						newCoef = ((MonomInt) mono.get(i)).getCoef() + ((MonomInt) mono.get(j)).getCoef();
						mono.remove(j);
						((MonomInt) mono.get(i)).setCoef(newCoef);
					} else {
						if (mono.get(i) instanceof MonomDoouble && mono.get(j) instanceof MonomDoouble) {
							coef = ((MonomDoouble) mono.get(i)).getCoef() + ((MonomDoouble) mono.get(j)).getCoef();
							mono.remove(j);
							((MonomDoouble) mono.get(i)).setCoef(coef);
						} else {
							if (mono.get(i) instanceof MonomDoouble && mono.get(j) instanceof MonomInt) {
								coef = ((MonomDoouble) mono.get(i)).getCoef() + ((MonomInt) mono.get(j)).getCoef();
								mono.remove(j);
								((MonomDoouble) mono.get(i)).setCoef(coef);
							} else {
								coef = ((MonomInt) mono.get(i)).getCoef() + ((MonomDoouble) mono.get(j)).getCoef();
								mono.remove(j);
								((MonomDoouble) mono.get(i)).setCoef(coef);

							}

						}
					}

				} else
					j++;
			}
		}

	}

	/**
	 * Method deleteZeroCoef is used to search in the monom's list and delete
	 * the ones that have the coefficient equals with zero
	 * 
	 * @param result
	 *            represent the polinom from which we want to remove monoms with
	 *            zero coefficient
	 * @param mono list of monoms
	 * @return the new list of monoms after removing zero coefficients
	 */
	public List<Monom> deleteZeroCoef(Polynom result) {
		int i;

		List<Monom> mono = result.getElement();

		//m and md are used to recognize which type of monom is 
		MonomInt m = new MonomInt();
		MonomDoouble md = new MonomDoouble();
		for (i = 0; i < mono.size(); i++) {

			if (mono.get(i) instanceof MonomInt) {
				m = (MonomInt) mono.get(i);
				if (m.getCoef() == 0) {
					mono.remove(i);
					i--;
				}
			} else {
				md = (MonomDoouble) mono.get(i);
				if (md.getCoef() == (double) 0) {
					mono.remove(i);
					i--;
				}
			}
		}

		return mono;
	}

	/**
	 * Method setElement add a monom to the monom list
	 * @param element the new monom for the list
	 */
	public void setElement(Monom element) {
		monomElement.add(element);
	}

	/**
	 *  Method  getElement
	 * @return monomElement the list of monoms
	 */
	public List<Monom> getElement() {

		return monomElement;
	}

	/**
	 * Method setMonomList sets a list of monoms
	 * @param monom the list of monoms
	 */
	public void setMonomList(List<Monom> monom) {
		monomElement = monom;

	}

	/**
	 * Method sortElement sort ascending the monoms from list
	 */
	public void sortElement() {
		Collections.sort(monomElement);

	}

}
