package gui;/*
			import org.json.simple.JSONObject;
			import org.json.simple.JSONArray;
			import org.json.simple.parser.ParseException;*/

//import com.fasterxml.jackson.core.JsonParseException;

//import org.json.simple.parser.JSONParser;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.json.simple.parser.ParseException;

import managewords.FacadePattern;
import managewords.ManageDictionary;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import storeword.*;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DictionaryInterface {

	private JFrame frame;
	private JTextField textFieldForWord;
	private JTextField txtWordtoadd;
	private JTextField txtSynonim;
	private DefaultTableModel modelForSynonims;
	private JTable tableForSynonims;
	private Map<String, ArrayList<String>> dictionary;
	private FacadePattern fascadePattern;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DictionaryInterface window = new DictionaryInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws ParseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws JsonParseException
	 */
	public DictionaryInterface() throws FileNotFoundException, IOException, ParseException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws ParseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws JsonParseException
	 */
	private void initialize() throws FileNotFoundException, IOException, ParseException {

		fascadePattern = new FacadePattern();
		dictionary = new HashMap<String, ArrayList<String>>();
		/*
		 * try { manageWords = new ManageDictionary(); } catch (ParseException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					fascadePattern.serialize();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frame.getContentPane().setBackground(new Color(165, 42, 42));
		frame.setBounds(100, 100, 720, 369);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panelForDisplayAllWords = new JPanel();
		panelForDisplayAllWords.setBackground(new Color(250, 250, 210));
		panelForDisplayAllWords.setBounds(10, 173, 684, 151);
		frame.getContentPane().add(panelForDisplayAllWords);
		panelForDisplayAllWords.setLayout(null);

		JPanel panelForWord = new JPanel();
		panelForWord.setBackground(new Color(250, 250, 210));
		panelForWord.setBounds(10, 11, 684, 151);
		frame.getContentPane().add(panelForWord);
		panelForWord.setLayout(null);

		JLabel lblSearchWord = new JLabel("Search word:");
		lblSearchWord.setBounds(10, 11, 100, 14);
		panelForWord.add(lblSearchWord);
		// region Description
		textFieldForWord = new JTextField();
		textFieldForWord.setBounds(108, 8, 283, 20);
		panelForWord.add(textFieldForWord);
		textFieldForWord.setColumns(10);
		// endregion
		Object[] columns = { " Word  ", "Synonym" };
		modelForSynonims = new DefaultTableModel(columns, 0);
		modelForSynonims = setModelForTableOfSynonuns();
		tableForSynonims = new JTable(modelForSynonims);
		tableForSynonims.setBorder(new LineBorder(new Color(244, 164, 96)));
		tableForSynonims.setVisible(true);
		tableForSynonims.setForeground(Color.BLACK);
		tableForSynonims.setBackground(new Color(250, 250, 210));
		tableForSynonims.setRowHeight(30);
		panelForDisplayAllWords.setVisible(false);
		Font font = new Font("", 1, 12);
		panelForDisplayAllWords.setLayout(null);
		tableForSynonims.setFont(font);
		tableForSynonims.setBounds(0, 0, panelForDisplayAllWords.getWidth() - 20, 140);
		JScrollPane scroll = new JScrollPane(tableForSynonims);
		scroll.setBounds(10, 5, panelForDisplayAllWords.getWidth() - 20, 140);
		panelForDisplayAllWords.add(scroll);
		frame.getContentPane().add(panelForDisplayAllWords);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> synonymList = new ArrayList<>();
				Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
				String word = null;
				int index = 0;
				word = textFieldForWord.getText();
				String part1, part2 = null;
				if (word.contains("?")) {
					index = word.indexOf("?");
					part1 = word.substring(0, index);
					part2 = word.substring(index + 1, word.length());
					System.out.println("part 1" + part1 + " part 2 " + part2);
					synonymList = fascadePattern.searchWordWithMissingCharacter(part1, part2);
					if (synonymList == null)
						JOptionPane.showMessageDialog(null, "Nu s-a gasit");
					else
						JOptionPane.showMessageDialog(null, synonymList);
				} else if (word.contains("*")) {
					index = word.indexOf("*");
					word = word.substring(0, index);
					map = fascadePattern.searchWordByLetter(word);

					Iterator<Entry<String, ArrayList<String>>> it = map.entrySet().iterator();
					Map.Entry<String, ArrayList<String>> entry;
					while (it.hasNext()) {
						entry = (Entry<String, ArrayList<String>>) it.next();
						System.out.println("Cuvantul : " + entry.getKey() + " Sinonimele : " + entry.getValue());
					}

					if (map == null)
						JOptionPane.showMessageDialog(null, "Nu s-a gasit");
					else
						JOptionPane.showMessageDialog(null, map);
				} else {
					synonymList = fascadePattern.searchWordByName(word);
					if (synonymList == null)
						JOptionPane.showMessageDialog(null, "Nu s-a gasit");
					else
						JOptionPane.showMessageDialog(null, synonymList);
				}

			}
		});
		btnSearch.setBackground(new Color(102, 205, 170));
		btnSearch.setBounds(10, 56, 89, 23);
		panelForWord.add(btnSearch);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String word;
				int selRow = tableForSynonims.getSelectedRow();
				if (selRow != -1) {

					word = (String) (tableForSynonims.getModel()).getValueAt(selRow, 0);
					fascadePattern.deleteWord(word);
					// modelForSynonims.removeRow(selRow);
					modelForSynonims = setModelForTableOfSynonuns();
				}
			}
		});

		btnDelete.setForeground(new Color(255, 255, 255));
		btnDelete.setBackground(new Color(0, 0, 0));
		btnDelete.setBounds(135, 108, 89, 23);
		panelForWord.add(btnDelete);

		JButton btnDisplay = new JButton("Display");
		btnDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelForDisplayAllWords.setVisible(true);
			}
		});
		btnDisplay.setBackground(new Color(248, 248, 255));
		btnDisplay.setBounds(10, 108, 89, 23);
		panelForWord.add(btnDisplay);

		JButton btnAddNew = new JButton("Add new");
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String word;
				String synonym;
				String message;
				if (txtWordtoadd.getText().length() != 0 && txtSynonim.getText().length() != 0) {
					word = txtWordtoadd.getText().replaceAll("\\s+", "");
					synonym = txtSynonim.getText().replaceAll("\\s+", "");
					message = fascadePattern.addWord(word, synonym);
					JOptionPane.showMessageDialog(null, message);
					modelForSynonims = setModelForTableOfSynonuns();
				} else
					JOptionPane.showMessageDialog(null, "Fill all the fields");

			}
		});
		btnAddNew.setBackground(new Color(95, 158, 160));
		btnAddNew.setBounds(420, 82, 89, 23);
		panelForWord.add(btnAddNew);

		txtWordtoadd = new JTextField();
		txtWordtoadd.setText("WordToAdd");
		txtWordtoadd.setBounds(555, 56, 96, 20);
		panelForWord.add(txtWordtoadd);
		txtWordtoadd.setColumns(10);

		txtSynonim = new JTextField();
		txtSynonim.setText("Synonim");
		txtSynonim.setBounds(555, 105, 96, 20);
		panelForWord.add(txtSynonim);
		txtSynonim.setColumns(10);
	}

	DefaultTableModel setModelForTableOfSynonuns() {
		dictionary = fascadePattern.getDictionary();

		Vector<String> row;

		if (modelForSynonims.getRowCount() > 0) {
			for (int i = modelForSynonims.getRowCount() - 1; i > -1; i--) {
				modelForSynonims.removeRow(i);
			}
		}

		Iterator it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			row = new Vector<>();
			row.add(entry.getKey());

			Object osdfjndsf;

			String list = new String();
			for (String s : entry.getValue()) {
				list += " " + s;
			}
			row.add(list);
			modelForSynonims.addRow(row);
		}
		return modelForSynonims;

	}
}
