package storeword;

import java.awt.List;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.io.Serializable;/*
							import com.fasterxml.jackson.core.*;
							import com.fasterxml.jackson.core.type.TypeReference;
							import com.fasterxml.jackson.core.JsonParseException;
							import com.fasterxml.jackson.core.JsonGenerationException;
							import com.fasterxml.jackson.core.type.TypeReference;*/
import java.io.Writer;

import org.codehaus.jackson.map.JsonMappingException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import org.codehaus.jackson.map.ObjectMapper;
public class SynonimsDictionary {

	private static Map<String, ArrayList<String>> dictionary;

	private SynonimsDictionary() {
	}

	private static Map<String, ArrayList<String>> deserialize()
			throws FileNotFoundException, IOException, ParseException {
		Map<String, ArrayList<String>> dictionary;
		JSONParser parser = new JSONParser();
		dictionary = new HashMap<String, ArrayList<String>>();
		FileReader file = new FileReader("Dictionar");

		// preluare informatie din fiser
		Object obj = parser.parse(file);

		JSONObject jsonObject;
		String word;
		ArrayList<String> synonymList = new ArrayList<String>();
		JSONArray jsonArray;
		JSONObject j2;

		Integer id = 0;
		if (obj != null) {
			word = new String();
			jsonObject = (JSONObject) obj;

			for (Object s : jsonObject.keySet()) {
				j2 = (JSONObject) jsonObject.get(s.toString());

				word = (String) j2.get("word");
				synonymList = (ArrayList<String>) j2.get("synonymList");
				dictionary.put(word, synonymList);
			}
		}
		return dictionary;
	}

	public static Map<String, ArrayList<String>> getDictionary() {
		if (dictionary == null) {
			dictionary = new HashMap<String, ArrayList<String>>();
			try {
				dictionary = deserialize();
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}
		}
		return dictionary;
	}
}
