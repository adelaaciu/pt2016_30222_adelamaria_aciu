package storeword;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public interface ManageDictionaryProc {

	/**
	 * @pre entry.getKey().equals(searchedWord)
	 * @post entry.getValue();
	 * 
	 */
	public ArrayList<String> searchWordByName(String searchedWord);

	/**
	 * @pre entry.getKey().startsWith(searchedWordFirstPart) &&
	 *      entry.getKey().endsWith(searchedWordSecondPart))
	 * @post entry.getValue();
	 * 
	 */
	public ArrayList<String> searchWordWithMissingCharacter(String searchedWordFirstPart,
			String searchedWordSecondPart);

	/**
	 * @pre entry.getKey().startsWith(searchedWord)
	 * @post entry.getValue();
	 * 
	 */
	public ArrayList<String> searchWordByLetter(String searchedWord);

	/**
	 * @pre entry.getKey().equals(searchedWord)
	 * @post dictionary.remove(searchedWord);
	 */
	public void deleteWord(String searchedWord);

	/**
	 * @pre entry.getKey().equals(word)
	 * @post entry.getValue().add(synonym);
	 * @post dictionary.put(word, synonymList);
	 */
	public String addWord(String word, String synonym);

	/**
	 * @pre ts.hasNext();
	 * @post searchWordByName(entry.getValue().get(i).toString()) == null
	 * 
	 */
	public boolean wellFormed();

}
