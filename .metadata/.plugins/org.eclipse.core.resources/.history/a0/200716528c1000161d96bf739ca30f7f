package storeword;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

//import com.fasterxml.jackson.core.JsonParseException;

public class ManageDictionary {

	private Map<String, ArrayList<String>> dictionary;
	private SynonimsDictionary dic;

	public ManageDictionary() throws FileNotFoundException, IOException, ParseException {
		dic = new SynonimsDictionary();
		dictionary = dic.getDictionary();
		// new HashMap<String, ArrayList<String>>();
	}

	/**
	 * Method search a word by it's whole name
	 * 
	 * @param searchedWord
	 *            word you want to search
	 * @return synonyms if found else null
	 */
	public ArrayList<String> searchWordByName(String searchedWord) {
		Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			entry = (Entry<String, ArrayList<String>>) it.next();
			if (entry.getKey().equals(searchedWord)) {
				return entry.getValue();
			}
		}

		return null;
	}

	/**
	 * Method searchWordWithMissingCharacter searches a word witch have a letter
	 * replaced by ?
	 * 
	 * @param searchedWordFirstPart
	 *            first part of the word
	 * @param searchedWordSecondPart
	 *            second part of the word
	 * @return synonyms if found else null
	 */
	public ArrayList<String> searchWordWithMissingCharacter(String searchedWordFirstPart,
			String searchedWordSecondPart) {

		Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			entry = (Entry<String, ArrayList<String>>) it.next();
			if (entry.getKey().startsWith(searchedWordFirstPart) && entry.getKey().endsWith(searchedWordSecondPart)) {
				return entry.getValue();
			}
		}

		return null;
	}

	/**
	 * Method searchWordByLetter searches a word using just a feww letters, the
	 * rest of it beeing marked by *
	 * 
	 * @param searchedWord
	 *            word user search
	 * @return synonyms if found else null
	 */
	public Map<String, ArrayList<String>> searchWordByLetter(String searchedWord) {
		Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
		boolean found = false;
		Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			entry = (Entry<String, ArrayList<String>>) it.next();
			if (entry.getKey().startsWith(searchedWord)) {
				found = true;
				map.put(entry.getKey(), entry.getValue());
			}
		}
		if (found == true)
			return map;
		else
			return null;
	}

	/**
	 * 
	 * @return true if even the synonym of a word is in the dictionary
	 */
	public boolean wellFormed() {

		int i;
		Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			entry = (Entry<String, ArrayList<String>>) it.next();
			for (i = 0; i < entry.getValue().size(); i++) {
				if (searchWordByName(entry.getValue().get(i).toString()) == null) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Method removes a certain word and it's synonyms
	 * 
	 * @param word
	 *            word you want to delete
	 */
	public void deleteWord(String word) {
		for(String s: dictionary.get(word)){
			dictionary.get(s).remove(word);
			if(dictionary.get(s).size() == 0){
				dictionary.remove(s);
			}
		}
	}

	/**
	 * Method adds a word in the dictionary or if it already exists it add a
	 * synonym to it's list
	 * 
	 * @param word
	 *            word you want to add
	 * @param synonym
	 *            synonym you want to add
	 * @return a certain message if the word exists and you add to actual list
	 *         or if it's a new word
	 */
	public String addWord(String word, String synonym) {
		ArrayList<String> synonymList;
		if (!dictionary.isEmpty()) {
			Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
			Map.Entry<String, ArrayList<String>> entry;
			while (it.hasNext()) {
				entry = (Entry<String, ArrayList<String>>) it.next();
				if (entry.getKey().equals(word)) {
					entry.getValue().add(synonym);

					synonymList = new ArrayList<>();
					synonymList.add(word);
					dictionary.put(synonym, synonymList);

					return "Added";
				}
			}
		}
		synonymList = new ArrayList<>();
		synonymList.add(synonym);
		dictionary.put(word, synonymList);

		synonymList = new ArrayList<>();
		synonymList.add(word);
		dictionary.put(synonym, synonymList);
		return "Created";
	}

	public void serializeV2() throws IOException {
		int i;
		JSONObject obj = new JSONObject();
		JSONObject writeObj = new JSONObject();
		JSONArray list = new JSONArray();
		FileWriter file = null;
		try {
			file = new FileWriter("Dictionar");
			file.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int id = 0;
		Iterator<Entry<String, ArrayList<String>>> it = dictionary.entrySet().iterator();
		Map.Entry<String, ArrayList<String>> entry;
		while (it.hasNext()) {
			id++;
			list = new JSONArray();
			writeObj = new JSONObject();
			entry = (Entry<String, ArrayList<String>>) it.next();

			for (i = 0; i < entry.getValue().size(); i++) {
				// System.out.println("c " + i + " " + entry.getValue().get(i));
				list.add(entry.getValue().get(i));
			}

			writeObj.put("word", entry.getKey());
			writeObj.put("synonymList", list);
			obj.put(id, writeObj);

			// file.write(writeObj.toJSONString());

		}
		file.write(obj.toJSONString());
		file.close();
	}

	public Map<String, ArrayList<String>> getDictionary() {
		return dictionary;
	}
}
