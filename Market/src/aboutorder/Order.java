package aboutorder;

import java.io.Serializable;
import buyers.Customer;
import storage.Product;

/**
 * Class order represnet the order one client makes
 * 
 * @author Adela
 *
 */
public class Order implements Serializable {

	private Customer customer;
	private Product product;
	private Double totalPrice;

	/**
	 * 
	 * @param customer
	 *            client
	 * @param productthe
	 *            product one client orders
	 */
	public Order(Customer customer, Product product) {

		this.customer = customer;
		this.product = product;
		// this.department = new DPDept();
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double price) {
		this.totalPrice = price;

	}

	@Override
	public String toString() {
		return "Ordercustomer =" + customer + " product=" + product;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

}
