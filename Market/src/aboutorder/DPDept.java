package aboutorder;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

import buyers.Customer;
import storage.Product;

/**
 * Clas DPDept manages orders
 * 
 * @author Adela
 *
 */
public class DPDept implements Serializable {

	private TreeMap<Integer, Order> manageOrder;
	private Integer id;

	public DPDept() {
		manageOrder = new TreeMap<Integer, Order>();
		deserialize();
	}

	public TreeMap<Integer, Order> getManageOrder() {
		return manageOrder;
	}

	public void setManageOrder(TreeMap<Integer, Order> manageOrder) {
		this.manageOrder = manageOrder;
	}

	/**
	 * Add order
	 * 
	 * @param buyer
	 *            client
	 * @param ord
	 *            order you want to add
	 */
	public void addOrder(Order ord) {
		manageOrder.put(getId(), ord);
	}

	public Integer getId() {
		id = 0;
		for (Map.Entry<Integer, Order> o : manageOrder.entrySet()) {
			id = o.getKey();
		}
		id += 1;
		// System.out.println("ID: " + id + "\n");
		return id;

	}

	public Integer getId(Order order) {
		for (Map.Entry<Integer, Order> o : manageOrder.entrySet())
			if (order.equals(o.getValue())) {
				return o.getKey();
			}
		return 0;
	}

	/**
	 * Deserialization of datas
	 */
	public void deserialize() {
		FileInputStream fileIn;
		try {

			fileIn = new FileInputStream("OrderList");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			manageOrder = (TreeMap) in.readObject();
			fileIn.close();
			/*
			 * for (Map.Entry<Integer, Order> tree : manageOrder.entrySet()) {
			 * System.out.println(tree.getValue() + " id " + tree.getKey()); }
			 */

		} catch (Exception e) {
			System.out.println("Nu sunt comenzi");
			// e.printStackTrace();
		}
	}

}
