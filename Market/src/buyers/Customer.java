package buyers;

import java.io.Serializable;

import aboutorder.*;
import storage.Product;

/**
 * Class Customer represent a client
 * 
 * @author Adela
 *
 */
public class Customer implements Comparable, Serializable {
	private String custumerName;
	private String mail;

	/**
	 * The constructor is used to initialize the variables
	 * 
	 * @param name
	 *            Name of the customer
	 * @param number
	 *            The number of order
	 */
	public Customer(String name, String mail) {
		this.custumerName = name;
		this.mail = mail;

	}

	public String getCustumerName() {
		return custumerName;
	}

	public void setCustumerName(String custumerName) {
		this.custumerName = custumerName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "Customer : " + custumerName + " mail=" + mail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((custumerName == null) ? 0 : custumerName.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (custumerName == null) {
			if (other.custumerName != null)
				return false;
		} else if (!custumerName.equals(other.custumerName))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		return true;
	}

	@Override
	public int compareTo(Object arg) {
		Customer p;
		p = (Customer) arg;
		if (this.custumerName.compareTo(p.getCustumerName()) > 0)
			return 1;
		if (this.custumerName.compareTo(p.getCustumerName()) < 0)
			return -1;
		return 0;
	}

}
