package buyers;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * Class ManageCustomers manages customers list
 * 
 * @author Adela
 *
 */
public class ManageCustomers {

	private List<Customer> customer;

	/**
	 * Constructor used for initialize datas
	 */
	public ManageCustomers() {
		customer = new ArrayList<Customer>();
		deserialize();
		/*
		 * customer.add(new Customer("Alex", "alex_pk")); customer.add(new
		 * Customer("Ade", "adela_maria")); setCustomer(customer);
		 */
	}

	public List<Customer> getCustomer() {
		return customer;
	}

	public void setCustomer(List<Customer> customer) {
		this.customer = customer;
	}

	/**
	 * Display list of customers
	 */
	public void displayList() {
		for (Customer c : customer) {
			System.out.println(c.toString());
		}
	}

	public Customer getCustomer(String customerName, String mail) {
		for (Customer c : customer) {

			if (c.getCustumerName().equals(customerName) && c.getMail().equals(mail))
				return c;
		}

		return null;
	}

	/**
	 * Get datas from file
	 */
	public void deserialize() {
		FileInputStream fileIn;
		try {
			fileIn = new FileInputStream("CustomersList");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			customer = (List<Customer>) in.readObject();
			/*
			 * for (Customer c : customer) { System.out.println(c + "\n"); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
