package test;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Map;

import storage.*;

public class Tests {

	@Test
	public void testAddProduct() {
		ManageWarehouse wearhouse = new ManageWarehouse();
		Product laptop = new Laptop("Laptop Toshiba", 2.0, 2, "good", 100);
		boolean added = false;
		added=wearhouse.addProduct(laptop, laptop.getCantity());
		assertTrue(added);
	}

}
