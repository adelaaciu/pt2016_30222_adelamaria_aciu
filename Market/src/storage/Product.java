
package storage;

import java.io.Serializable;

/**
 * Class Product represent one product from a treemap
 * 
 * @author Adela
 *
 */
public class Product implements Comparable, Serializable {
	private String productName;
	private Double price;
	private Integer quantity;
	private String description;
	private Integer id;

	public Product() {

	}

	/**
	 * Constructor used to initialize components
	 * @param name
	 *            name of product
	 * @param price
	 *            price of product
	 * @param quantity
	 *            quantity of product
	 * @param description
	 *            description of product
	 * @param id
	 *            id of product
	 */
	public Product(String name, double price, int quantity, String description, Integer id) {
		this.productName = name;
		this.price = price;
		this.quantity = quantity;
		this.description = description;
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getCantity() {
		return quantity;
	}

	public void setID(Integer id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setCantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		// setTotalPrice(this);
		return "Product productName: " + productName + " price=" + price + " quantity=" + quantity + " description "
				+ description; // + " total price " + this.getTotalPrice();
	}

	@Override
	public int compareTo(Object arg) {
		Product p;
		p = (Product) arg;
		if (this.productName.compareTo(p.getProductName()) > 0)
			return 1;
		if (this.productName.compareTo(p.getProductName()) < 0)
			return -1;
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

}
