package storage;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import buyers.Customer;

public class Warehouse implements Serializable {

	private TreeMap<Product, Integer> stock;
	private static final Integer limit = 100;
	private int laptopStock;
	private int mouseStock;
	private int headPhonesStock;

	public Warehouse() {
		stock = new TreeMap<Product, Integer>();
		laptopStock = 40;
		mouseStock = 30;
		headPhonesStock = 30;
		deserialize();
		// setElements();
	}

	/**
	 * Sets initial elements for Warehouse
	 */
	public void setElements() {

		Product p = new Product("Laptop Assus", 3.1, 41, "Desc1", 1);
		Product p1 = new Product("Laptop ", 3.0, 42, "Desc1", 2);
		Product p2 = new Product("Laptop 2", 3.0, 34, "Desc2", 3);
		Product p3 = new Laptop("Laptop1 ", 3.0, 4, "Desc3", 4);
		Product p4 = new Laptop("Laptop2 ", 3.0, 45, "Desc4", 5);
		Product p5 = new Laptop("Laptop3 ", 3.0, 48, "Desc5", 6);
		Product p6 = new HeadPhones("HeadPhones1", 3.0, 4, "Desc6", 6);
		Product p7 = new HeadPhones("HeadPhones2", 3.0, 4, "Desc7", 7);
		Product p8 = new HeadPhones("HeadPhones3", 3.0, 4, "Desc8", 8);
		Product p9 = new HeadPhones("HeadPhones5", 3.0, 4, "Desc9", 9);
		Product p10 = new HeadPhones("HeadPhones4", 3.0, 4, "Desc10", 10);
		Product p11 = new Mouse("Name", 49, 8, "Desc11", 11);
		Product p12 = new Mouse("Mouse", 3.0, 4, "Desc12", 12);
		Product p13 = new Mouse("Mouse", 3.0, 4, "Desc13", 13);
		Product p14 = new Mouse("Mouse", 3.0, 4, "Desc14", 14);
		stock.put(p, p.getCantity());
		stock.put(p1, p1.getCantity());
		stock.put(p2, p2.getCantity());
		stock.put(p3, p3.getCantity());
		stock.put(p4, p4.getCantity());
		stock.put(p5, p5.getCantity());
		stock.put(p6, p6.getCantity());
		stock.put(p7, p7.getCantity());
		stock.put(p8, p8.getCantity());
		stock.put(p9, p9.getCantity());
		stock.put(p10, p10.getCantity());
		stock.put(p11, p11.getCantity());
		stock.put(p12, p12.getCantity());
		stock.put(p13, p13.getCantity());
		stock.put(p14, p14.getCantity());
		// displayTree();
	}

	public Integer getLimit() {
		return this.limit;
	}

	public int getLaptopStock() {
		return laptopStock;
	}

	public void setLaptopStock(int laptopStock) {
		this.laptopStock = laptopStock;
	}

	public int getMouseStock() {
		return mouseStock;
	}

	public void setMouseStock(int mouseStock) {
		this.mouseStock = mouseStock;
	}

	public int getHeadPhonesStock() {
		return headPhonesStock;
	}

	public void setHeadPhonesStock(int headPhonesStock) {
		this.headPhonesStock = headPhonesStock;
	}

	public TreeMap<Product, Integer> getStock() {
		return stock;
	}

	public void setStock(TreeMap<Product, Integer> stock) {
		this.stock = stock;
	}

	/**
	 * Get datas from file
	 */
	public void deserialize() {
		FileInputStream fileIn;

		try {

			fileIn = new FileInputStream("ProductList");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			stock = (TreeMap<Product, Integer>) in.readObject();
			fileIn.close();
			/*
			 * for (Map.Entry<Product, Integer> tree : stock.entrySet()) {
			 * System.out.println("Produs : " + tree.getKey() + " Valoare " +
			 * tree.getValue() + "\n"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
