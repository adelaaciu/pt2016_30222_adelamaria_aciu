package storage;

import java.util.Map;
import java.util.TreeMap;

import javax.swing.JOptionPane;

public class ManageWarehouse {
	private TreeMap<Product, Integer> stock;
	private Integer limit;
	private int laptopStock;
	private int mouseStock;
	private int headPhonesStock;
	private int curentLaptopStock;
	private int curentMouseStock;
	private int curentHeadPhonesStock;
	private Warehouse warehouse;

	/**
	 * Initialize components
	 */
	public ManageWarehouse() {
		stock = new TreeMap<>();
		warehouse = new Warehouse();
		laptopStock = warehouse.getLaptopStock();
		mouseStock = warehouse.getMouseStock();
		headPhonesStock = warehouse.getHeadPhonesStock();
		stock = warehouse.getStock();
		limit = 100;
	}

	/**
	 * Add to wareHouse a product
	 * 
	 * @param product
	 *            new product you want to add
	 * @param value
	 *            for stock
	 */
	public boolean addProduct(Product product, Integer value) {
		boolean exists = false;
		if (isOverflow(value) == false)
			if (stock != null) {
				for (Map.Entry<Product, Integer> tree : stock.entrySet()) {
					if (product.equals(tree.getKey())) {
						exists = true;
						return false;
					}
				}
			}
		stock.put(product, value);
		return true;
	}

	/**
	 * Lower the stock for a product
	 * 
	 * @param product
	 *            product from which you want to remove a certain quantity
	 * @param value
	 *            value you want to remove
	 */
	public void decrementElement(Product product, Integer value) {

		if (stock != null)
			for (Map.Entry<Product, Integer> tree : stock.entrySet()) {
				if (product.equals(tree.getKey()))
					if ((tree.getValue() - value) > 0) {
						stock.replace(product, tree.getValue() - value);
						break;
					} else if ((tree.getValue() - value) == 0)
						stock.remove(tree);

			}
		// setStock(stock);
	}

	/**
	 * Remove a product from binary search tree
	 * 
	 * @param product
	 *            the product you want to remove
	 */
	public void removeProduct(Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				stock.remove(prod.getKey());
				setStock(stock);
				break;
			}
		}

	}

	/**
	 * Tests if the warehouse if full or not when you want to add a product
	 * 
	 * @return true if the warehouse is overflow and false in the other case
	 */
	public boolean isOverflow(Integer value) {

		curentHeadPhonesStock = getNoOfHeadPhones();
		curentLaptopStock = getNoOfLaptops();
		curentMouseStock = getNofMouse();
		int noOfProducts = curentHeadPhonesStock + curentLaptopStock + curentMouseStock;

		if (noOfProducts + value > limit) {
			System.out.println("limit");
			return true;
		}
		if (curentHeadPhonesStock + value > headPhonesStock) {
			System.out.println("head");
			return true;
		}
		if (curentLaptopStock + value > laptopStock) {
			System.out.println("lap");
			return true;
		}
		if (curentMouseStock + value > mouseStock) {
			System.out.println("mouse");
			return true;
		}

		return false;

	}

	public Integer getNoOfHeadPhones() {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof HeadPhones)
				curentHeadPhonesStock++;
		}

		return curentHeadPhonesStock;
	}

	public Integer getNoOfLaptops() {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof HeadPhones)
				curentLaptopStock++;
		}

		return curentLaptopStock;
	}

	public Integer getNofMouse() {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof HeadPhones)
				curentMouseStock++;
		}

		return curentMouseStock;
	}

	/**
	 * Tests if there are not enough products in stock
	 * 
	 * @param product
	 *            product you want to buy
	 * @param cantity
	 *            quantity you want to buy
	 * @return true if you can't buy the product because the quantity is too
	 *         much and false if you can buy the product
	 */
	public boolean isUnderStock(Product product, Integer cantity) {

		if ((product.getCantity() - cantity) < 0)
			return true;

		return false;
	}

	/**
	 * Rename product
	 * 
	 * @param name
	 *            actual name
	 * @param replaceName
	 *            name after replace
	 * @return BST of products
	 */
	public void renameProduct(String replaceName, Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				prod.getKey().setProductName(replaceName);
			}
		}
	}

	/**
	 * Change the description of the product
	 * 
	 * @param description
	 *            new product description
	 * @param name
	 *            name of the product
	 * @return BST of products
	 */
	public void setDescriptionProduct(String description, Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				prod.getKey().setDescription(description);
			}

		}
	}

	/**
	 * Change product id
	 * 
	 * @param id
	 *            new id for produc t
	 * @param name
	 *            name of the product
	 * @return BST of products
	 */
	public void newIDProduct(int id, Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				prod.getKey().setID(id);
			}
		}
	}

	/**
	 * Set new price for product
	 * 
	 * @param price
	 *            new price
	 * @param name
	 *            name of the product
	 * @return BST of products
	 */
	public void newPriceForProduct(Double price, Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				prod.getKey().setPrice(price);
			}
		}
	}

	/**
	 * Change quantity for product
	 * 
	 * @param value
	 *            new quantity for the product
	 * @param name
	 *            name of the product
	 * @return BST of products
	 */
	public void newQuantityForProduct(int value, Product p) {

		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().equals(p)) {
				prod.getKey().setCantity(value);
				break;
			}

		}
	}

	public TreeMap<Product, Integer> getStock() {
		return stock;
	}

	public void setStock(TreeMap<Product, Integer> stock) {
		this.stock = stock;
	}

	public Product getElement(int i) {

		int k = 0;
		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (k == i) {
				return prod.getKey();
			}
			k++;
		}
		return null;

	}

	/**
	 * Display elements of tree map
	 */
	public void displayTree() {
		for (Map.Entry<Product, Integer> tree : stock.entrySet()) {

			System.out.println("Produs : " + tree.getKey() + " Valoare " + tree.getValue() + "\n");
		}
	}

	public Product getProduct(String name) {

		int k = 0;
		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey().getProductName().equals(name)) {
				return prod.getKey();
			}

		}
		return null;

	}

	public TreeMap<Product, Integer> getLaptop() {
		TreeMap<Product, Integer> stockLaptop = new TreeMap<>();
		int k = 0;
		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof Laptop) {
				stockLaptop.put(prod.getKey(), prod.getValue());
			}

		}
		return stockLaptop;

	}

	public TreeMap<Product, Integer> getHeadPhones() {
		TreeMap<Product, Integer> stockHeadPhones = new TreeMap<>();
		int k = 0;
		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof HeadPhones) {
				stockHeadPhones.put(prod.getKey(), prod.getValue());
			}

		}
		return stockHeadPhones;

	}

	public TreeMap<Product, Integer> getMouse() {
		TreeMap<Product, Integer> stockMouse = new TreeMap<>();
		int k = 0;
		for (Map.Entry<Product, Integer> prod : stock.entrySet()) {
			if (prod.getKey() instanceof Mouse) {
				stockMouse.put(prod.getKey(), prod.getValue());
			}

		}
		return stockMouse;

	}

}
