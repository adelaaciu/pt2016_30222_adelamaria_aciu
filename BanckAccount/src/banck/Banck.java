package banck;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import customer.Person;

public class Banck implements BanckProc {

	private Person customer;
	private Account account;
	private HashMap<Person, Account> manageAccount;

	public Banck() {
		// manageAccount = new HashMap<>();
		deserialize();
	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void removePerson(Person p, Account accont) {
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();

			if ((entry.getValue()).getId() == accont.getId()) {
				(entry.getValue()).removeObserver(p);
				accont.notifyObservers();
				break;
			}

		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void addPerson(Person p, Account accont) {
		// messajeForUser = new String("Person exist");
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()).getId() == accont.getId()) {
				(entry.getValue()).registerObserver(p);
				(entry.getValue()).notifyObservers();
			}

		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	public Person searchAnotherHolder(Account account, Person holder) {
		Person newHolder = null;
		if (account instanceof SavingAccount) {
			newHolder = ((SavingAccount) account).searchObserver(holder);
		}

		else {
			newHolder = ((SpendingAccount) account).searchObserver(holder);
		}

		return newHolder;

	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void removeHolder(Person holder, Account account) {
		Iterator it = manageAccount.entrySet().iterator();
		Person newHolder = null;
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getKey()).equals(holder)) {
				newHolder = searchAnotherHolder(entry.getValue(), entry.getKey());

				if (newHolder != null) {
					account = entry.getValue();
					manageAccount.remove(entry.getKey());
					account.removeObserver(entry.getKey());
					manageAccount.put(newHolder, account);
					account.notifyObservers();
					break;
				}

				else {
					manageAccount.remove(entry.getKey());
					account.notifyObservers();
					break;
				}
			}

		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void addHolder(Person holder, Account account) {
		boolean found = false;
		Iterator it = manageAccount.entrySet().iterator();

		Map.Entry<Person, Account> entry = null;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if (entry.getValue().getId() == account.getId()) {
				found = true;
				account = entry.getValue();
				break;
			}

		}

		if (found == true) {
			entry.getValue().registerObserver(holder);
			manageAccount.remove(entry.getKey());
			manageAccount.put(holder, account);
			entry.getValue().notifyObservers();
		}

	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void removeAccount(Account account) {

		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()).getId() == account.getId()) {
				manageAccount.remove(entry.getKey());
				account.notifyObservers();
			}
		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	@Override
	public void addAcount(Person person, Account account) {

		int nr = 0;
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getKey()).equals(person)) {
				nr++;
			}
		}
		if (nr < 2) {
			manageAccount.put(person, account);
			account.registerObserver(person);
			account.notifyObservers();
		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	public HashMap<Person, Account> getManageAccount() {
		return manageAccount;
	}

	public void setManageAccount(HashMap<Person, Account> manageAccount) {
		this.manageAccount = manageAccount;
	}

	/**
	 * @invariant isWellFormed()
	 */
	public void retragereNumerar(Account account, float value) {
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()).getId() == account.getId()) {

				entry.getValue().takeMoney(value);
				entry.getValue().notifyObservers();
			}
		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	public void addMoneyToCount(Account account, float value) {
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()).getId() == account.getId()) {
				entry.getValue().addMoney(value);
				entry.getValue().notifyObservers();
			}
		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	public void addRate(Account account) {
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()).getId() == account.getId()) {
				((SavingAccount) entry.getValue()).setNewValue();
				entry.getValue().notifyObservers();
			}
		}
	}

	/**
	 * @invariant isWellFormed()
	 */
	protected boolean isWellFormed() {
		Iterator it = manageAccount.entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if ((entry.getValue()) == null && entry.getValue() != null) {
				return false;
			}
			if ((entry.getValue()) != null && entry.getValue() == null) {
				return false;
			}
		}

		return true;
	}

	public void deserialize() {
		FileInputStream fileIn;

		try {

			fileIn = new FileInputStream("BanckStore");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			manageAccount = (HashMap<Person, Account>) in.readObject();
			fileIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
