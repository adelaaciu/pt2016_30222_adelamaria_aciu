package banck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import customer.Person;

public abstract class Account implements Observer,Serializable {

	protected String id;
	protected float money;
	protected String type;
	protected ArrayList<Observer> observers;

	public Account(String type, float value) {
		this.type = type;
		this.money = value;
		observers = new ArrayList<Observer>();
	}

	public abstract void takeMoney(float value);

	public abstract void addMoney(float value);

	public abstract void setId(String id);

	public abstract String getId();

	public void notifyObservers() {
		System.out.println("Notifying observers");
		for (Observer ob : observers) {
			ob.update(ob, this.money);
		}

	}

	public void registerObserver(Observer observer) {
		int i;
		boolean found = false;
		for (i = 0; i < observers.size(); i++) {
			if (((Person) (observers.get(i))).getcnp() == ((Person) observer).getcnp()) {
				found = true;
				break;
			}
		}
		if (found == false)
			observers.add(observer);

	}

	public void removeObserver(Observer observer) {
		System.out.println("helo");
		int i;
		for (i = 0; i < observers.size(); i++) {
			System.out.println((observers.get(i)));
		}
		observers.remove(observer);
	}

	public ArrayList<Observer> getObservers() {
		return observers;
	}

	public float getMoney() {
		return money;
	}

	public String getType() {
		return type;
	}

}
