package banck;

import java.util.ArrayList;

import customer.Person;

public class SpendingAccount extends Account {

	public SpendingAccount(String type, float value) {
		super(type, value);
	}

	@Override
	public void takeMoney(float value) {
		System.out.println("Intra");
		if (money > value) {
			this.money = this.money - value;
			this.notifyObservers();
		}
	}

	@Override
	public void addMoney(float value) {
		if (value > 0) {
			this.money += value;
			this.notifyObservers();
		}
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	public ArrayList<Observer> getObservers() {
		return observers;
	}

	public void setObservers(ArrayList<Observer> observers) {
		this.observers = observers;
	}

	public Person searchObserver(Person p) {
		int i;
		Person newHolder = null;
		for (i = 0; i < observers.size(); i++) {
			if (((Person) (observers.get(i))).equals(p))
				if ((i + 1) < observers.size()) {
					newHolder = (Person) (observers.get(i + 1));
					break;
				}

		}
		return newHolder;
	}

	@Override
	public void update(Observer ob, float money) {
		// TODO Auto-generated method stub

	}
}
