package banck;

import customer.*;
import java.util.ArrayList;

public class SavingAccount extends Account {

	public final static float GAIN = (float) 0.5;

	public final static int MIN_VALUE = 100;

	public SavingAccount(String type, float value) {
		super(type, value);
	}

	@Override
	public void takeMoney(float value) {

		float take = 0;

		take = this.money - value;
		if (take == 0) {
			this.money = 0;
			this.notifyObservers();
		}

	}

	@Override
	public void addMoney(float value) {
		if (value > 0) {
			this.money += value;
			this.notifyObservers();
		}
	}

	@Override
	public void setId(String id) {
		this.id = id;

	}

	public void setNewValue() {

		this.money += this.money * this.GAIN;
		this.notifyObservers();

	}

	@Override
	public String getId() {
		return this.id;
	}

	public ArrayList<Observer> getObservers() {
		return observers;
	}

	public Person searchObserver(Person p) {
		int i;
		Person newHolder = null;
		for (i = 0; i < observers.size(); i++) {
			if (((Person) (observers.get(i))).equals(p))
				if ((i + 1) < observers.size()) {
					newHolder = (Person) (observers.get(i + 1));
				}

		}
		return newHolder;
	}

	@Override
	public void update(Observer ob, float money) {
		// TODO Auto-generated method stub

	}
}