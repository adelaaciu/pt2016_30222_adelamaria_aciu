package banck;

import customer.Person;

public interface BanckProc {

	/**
	 * @pre (entry.getValue()).getId() == accont.getId()
	 * @post (entry.getValue()).removeObserver(p);
	 */
	public void removePerson(Person p, Account accont);

	/**
	 * @pre (entry.getValue()).getId() == accont.getId()
	 * @post (entry.getValue()).registerObserver(p)
	 */
	public void addPerson(Person p, Account accont);

	/**
	 * @pre (entry.getKey()).equals(holder)
	 * @post manageAccount.remove(entry.getKey())
	 */
	public void removeHolder(Person p, Account account);

	/**
	 * @pre entry.getValue().getId() == account.getId()
	 * @post manageAccount.remove(entry.getKey())
	 */
	public void addHolder(Person p, Account account);

	/**
	 * @pre (entry.getValue()).getId() == account.getId()
	 * @post manageAccount.remove(entry.getKey())
	 */
	public void removeAccount(Account account);

	/**
	 * @pre nr < 2
	 * @post account.registerObserver(person)
	 */
	public void addAcount(Person person, Account account);

}
