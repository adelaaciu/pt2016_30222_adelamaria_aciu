package test;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;

import banck.Account;
import banck.Banck;
import banck.SavingAccount;
import customer.Person;

public class Tests {
	Banck bank;
	HashMap<Person, Account> manageAccount;

	public void setUp() {
		bank = new Banck();
		manageAccount = new HashMap<>();
	}

	@Test
	public void testAddInHasMap() {
		setUp();
		int nr = 0;
		boolean justOne = false;
		Person person = new Person("Mirela", "Matei", 123524623);
		person.setId(1);
		Account account = new SavingAccount("Saving", 500);
		account.setId("sa" + 1);

		bank.addAcount(person, account);

		Iterator it = bank.getManageAccount().entrySet().iterator();
		Map.Entry<Person, Account> entry;
		boolean exists = false;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			nr++;
		}
		if (nr == 3)
			justOne = true;
		assertTrue(justOne);
	}
}
