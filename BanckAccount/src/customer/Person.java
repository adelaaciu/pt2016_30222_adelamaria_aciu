package customer;

import java.io.Serializable;

import banck.Observer;

public class Person implements Observer, Serializable {
	private String firstName;
	private String lastName;
	private int cnp;
	private int id;

	public Person(String firstName, String lastName, int cnp) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.cnp = cnp;
	}

	@Override
	public String toString() {
		return "First name : " + this.firstName + " Last name " + this.lastName + " cnp : " + cnp + " id " + this.id;

	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getcnp() {
		return cnp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cnp;
		result = prime * result + id;
		return result;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (cnp != other.cnp)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public void update(Observer ob, float moeny) {
		System.out.println(ob + " Bani " + moeny);

	}

}
