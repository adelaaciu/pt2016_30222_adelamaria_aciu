package userinterface;

import banck.*;
import customer.Person;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.JPanel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class UserInterface {

	private JFrame frame;
	private JTextField txtFirstname;
	private JTextField txtLastname;
	private JTextField txtCNP;
	private JTextField textFieldSumToAddToAccount;
	private JTextField txtValue;
	private JTextField txtType;
	private int idForCustomer;
	private String idForAccount;
	private Banck manageBanck;
	private DefaultTableModel modelForCustomers;
	private DefaultTableModel modelForAccounts;
	private JTable tableOfCustomers;
	private JTable tableOfAccount;
	private DefaultListModel<String> modelForJList;
	private JList JListPersons;
	private JTextField txtId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInterface window = new UserInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		manageBanck = new Banck();
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				serialization();
			}
		});
		frame.setBounds(100, 100, 1073, 559);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnAddPerson = new JButton("AddClient");
		btnAddPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String firsName = "", lastName = "";
				int cnp = 0;
				Person person = null;
				try {
					firsName = txtFirstname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert First Name");
				}

				try {
					lastName = txtLastname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					cnp = Integer.parseInt((txtCNP.getText()));
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					person = new Person(firsName, lastName, cnp);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				String id;
				String type;
				float money;
				Account cont = null;

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {

					id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}

					person = new Person(firsName, lastName, cnp);
					Random rnd = new Random();
					idForCustomer = rnd.nextInt(100);
					person.setId(idForCustomer);
					cont.setId(id);
					manageBanck.addPerson(person, cont);
				} else
					JOptionPane.showMessageDialog(null, "Selecteaza contul la care vrei sa adaugi observatorul");

			}
		});
		btnAddPerson.setBounds(35, 367, 125, 23);
		frame.getContentPane().add(btnAddPerson);

		JButton btnDeletePerson = new JButton("Delete Client");
		btnDeletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String firsName = "", lastName = "";
				int cnp = 0;
				Person person = null;
				try {
					firsName = txtFirstname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert First Name");
				}

				try {
					lastName = txtLastname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					cnp = Integer.parseInt((txtCNP.getText()));
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					person = new Person(firsName, lastName, cnp);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				String id;
				String type;
				float money;
				Account cont = null;

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {

					id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}
					cont.setId(id);
					person = new Person(firsName, lastName, cnp);

					person.setId(Integer.parseInt(txtId.getText()));
					cont.setId(id);

					manageBanck.removePerson(person, cont);

				} else
					JOptionPane.showMessageDialog(null, "Selecteaza contul de la care vrei sa stergi observatorul");

			}
		});
		btnDeletePerson.setBounds(35, 401, 125, 23);
		frame.getContentPane().add(btnDeletePerson);

		txtFirstname = new JTextField();
		txtFirstname.setText("FirstName");
		txtFirstname.setBounds(295, 368, 86, 20);
		frame.getContentPane().add(txtFirstname);
		txtFirstname.setColumns(10);

		txtLastname = new JTextField();
		txtLastname.setText("LastName");
		txtLastname.setBounds(295, 402, 86, 20);
		frame.getContentPane().add(txtLastname);
		txtLastname.setColumns(10);

		txtCNP = new JTextField();
		txtCNP.setText("1");
		txtCNP.setBounds(295, 433, 86, 20);
		frame.getContentPane().add(txtCNP);
		txtCNP.setColumns(10);

		JLabel lblFirstname = new JLabel("FirstName");
		lblFirstname.setBounds(188, 371, 70, 14);
		frame.getContentPane().add(lblFirstname);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(188, 405, 70, 14);
		frame.getContentPane().add(lblLastName);

		JLabel CNP = new JLabel("CNP");
		CNP.setBounds(188, 436, 97, 14);
		frame.getContentPane().add(CNP);

		JButton btnAdaugareInCont = new JButton("Adaugare in cont");
		btnAdaugareInCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String firsName = "", lastName = "";
				int cnp = 0;
				int personId;
				Person person = null;
				try {
					firsName = txtFirstname.getText();
					lastName = txtLastname.getText();
					cnp = Integer.parseInt((txtCNP.getText()));
					person = new Person(firsName, lastName, cnp);
					personId = Integer.parseInt(txtId.getText());
					person.setId(personId);

					String id;
					String type;
					float money;
					Account cont = null;
					float value = 0;

					int selRow = tableOfAccount.getSelectedRow();
					try {
						value = Float.parseFloat(textFieldSumToAddToAccount.getText());
					} catch (Exception arg) {
						JOptionPane.showMessageDialog(null, "Insert amount of money");
					}

					if (selRow != -1) {

						id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
						type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
						money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

						if (type.equals("Spending")) {
							cont = new SpendingAccount(type, money);
						} else {
							cont = new SavingAccount(type, money);
						}
						cont.setId(id);

						// cont.takeMoney(value);
						manageBanck.addMoneyToCount(cont, value);
					} else
						JOptionPane.showMessageDialog(null, "Select account");

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);
				} catch (Exception arg) {
					JOptionPane.showMessageDialog(null, "Completeaza toate campurile");
				}
			}
		});
		btnAdaugareInCont.setHorizontalAlignment(SwingConstants.LEFT);
		btnAdaugareInCont.setBounds(570, 432, 137, 23);
		frame.getContentPane().add(btnAdaugareInCont);

		JButton btnRetrag = new JButton("Retragere");
		btnRetrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String firsName = "", lastName = "";
				int cnp = 0;
				int personId;
				Person person = null;
				try {
					firsName = txtFirstname.getText();
					lastName = txtLastname.getText();
					cnp = Integer.parseInt((txtCNP.getText()));
					person = new Person(firsName, lastName, cnp);
					personId = Integer.parseInt(txtId.getText());
					person.setId(personId);

					String id;
					String type;
					float money;
					Account cont = null;
					float value = 0;

					int selRow = tableOfAccount.getSelectedRow();
					try {
						value = Float.parseFloat(textFieldSumToAddToAccount.getText());
					} catch (Exception arg) {
						JOptionPane.showMessageDialog(null, "Insert amount of money");
					}

					if (selRow != -1) {

						id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
						type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
						money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

						if (type.equals("Spending")) {
							cont = new SpendingAccount(type, money);
						} else {
							cont = new SavingAccount(type, money);
						}
						cont.setId(id);

						// cont.takeMoney(value);
						manageBanck.retragereNumerar(cont, value);
					} else
						JOptionPane.showMessageDialog(null, "Select account");

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);
				} catch (Exception arg) {
					JOptionPane.showMessageDialog(null, "Complete all fields");
				}

			}
		});
		btnRetrag.setBounds(570, 401, 110, 23);
		frame.getContentPane().add(btnRetrag);

		textFieldSumToAddToAccount = new JTextField();
		textFieldSumToAddToAccount.setText("$");
		textFieldSumToAddToAccount.setBounds(724, 416, 86, 20);
		frame.getContentPane().add(textFieldSumToAddToAccount);
		textFieldSumToAddToAccount.setColumns(10);

		JButton btnStergereCont = new JButton("Delete ");
		btnStergereCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id;
				String type;
				float money;
				Account cont = null;

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {

					id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}
					cont.setId(id);
					manageBanck.removeAccount(cont);

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);
				} else
					JOptionPane.showMessageDialog(null, "Alege contul care doresti sa il stergi");

			}
		});
		btnStergereCont.setBounds(929, 35, 87, 23);
		frame.getContentPane().add(btnStergereCont);

		JButton btnAddAccount = new JButton("Add ");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String type = "";
				float money = 0;
				Account cont = null;
				String firsName = "", lastName = "";
				int cnp = 0;
				Person person = null;

				try {
					type = (String) txtType.getText();
					money = Float.parseFloat((String) (txtValue.getText()));
					firsName = txtFirstname.getText();
					lastName = txtLastname.getText();
					cnp = Integer.parseInt((txtCNP.getText()));
					person = new Person(firsName, lastName, cnp);

					idForCustomer = setId(cnp);
					person.setId(idForCustomer);

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
						cont.setId("sp" + idForCustomer);
						manageBanck.addAcount(person, cont);
					} else {
						if (money > SavingAccount.MIN_VALUE) {
							cont = new SavingAccount(type, money);
							cont.setId("sa" + idForCustomer);
							manageBanck.addAcount(person, cont);
						} else
							JOptionPane.showMessageDialog(null, "Suma introdusa >= " + SavingAccount.MIN_VALUE);
					}

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Complete all fields from Account and from Holder");
				}

			}
		});
		btnAddAccount.setBounds(930, 185, 86, 23);
		frame.getContentPane().add(btnAddAccount);

		txtValue = new JTextField();
		txtValue.setText("Value");
		txtValue.setBounds(930, 83, 86, 20);
		frame.getContentPane().add(txtValue);
		txtValue.setColumns(10);

		txtType = new JTextField();
		txtType.setText("Type");
		txtType.setBounds(930, 132, 86, 20);
		frame.getContentPane().add(txtType);
		txtType.setColumns(10);

		JPanel panelForCustomers = new JPanel();
		panelForCustomers.setBounds(10, 11, 492, 334);

		JPanel panelForAccount = new JPanel();
		panelForAccount.setBounds(512, 11, 407, 374);
		frame.getContentPane().add(panelForAccount);
		panelForAccount.setLayout(null);

		/*
		 * Table for Customers
		 */
		// de agaugat modelul

		Object[] columns = { " CNP   ", "ID", "First Name", "Last Name" };
		modelForCustomers = new DefaultTableModel(columns, 0);

		modelForCustomers = setTable();
		tableOfCustomers = new JTable(modelForCustomers);
		tableOfCustomers.setVisible(true);
		tableOfCustomers.setForeground(Color.BLACK);
		tableOfCustomers.setBackground(Color.WHITE);
		tableOfCustomers.setRowHeight(30);
		Font font = new Font("", 1, 12);
		panelForCustomers.setLayout(null);
		tableOfCustomers.setFont(font);
		tableOfCustomers.setBounds(0, 0, 120, 100);
		JScrollPane scroll = new JScrollPane(tableOfCustomers);
		scroll.setBounds(10, 5, 472, 318);
		panelForCustomers.add(scroll);
		frame.getContentPane().add(panelForCustomers);

		Object[] columnsForAccount = { " ID", "Type", "Money" };
		modelForAccounts = new DefaultTableModel(columnsForAccount, 0);

		modelForAccounts = setTableForAccounts();
		tableOfAccount = new JTable(modelForAccounts);
		tableOfAccount.setVisible(true);
		tableOfAccount.setForeground(Color.BLACK);
		tableOfAccount.setBackground(Color.WHITE);
		tableOfAccount.setRowHeight(30);
		Font font2 = new Font("", 1, 12);
		panelForAccount.setLayout(null);
		tableOfAccount.setFont(font);
		tableOfAccount.setBounds(0, 0, 120, 100);
		JScrollPane scroll2 = new JScrollPane(tableOfAccount);
		scroll2.setBounds(10, 5, 387, 358);
		panelForAccount.add(scroll2);
		frame.getContentPane().add(panelForAccount);

		JButton btnDisplayPersons = new JButton("Display persons");
		btnDisplayPersons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {
					String id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					displayObservants(id);
				}

			}
		});
		btnDisplayPersons.setBounds(870, 415, 146, 23);
		frame.getContentPane().add(btnDisplayPersons);

		JButton buttonAddHolder = new JButton("Add Holder");
		buttonAddHolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String firsName = "", lastName = "";
				int cnp = 0;
				Person person = null;
				try {
					firsName = txtFirstname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert First Name");
				}

				try {
					lastName = txtLastname.getText();
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					cnp = Integer.parseInt((txtCNP.getText()));
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				try {
					person = new Person(firsName, lastName, cnp);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Insert Last Name");
				}

				String id;
				String type;
				float money;
				Account cont = null;

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {

					id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}

					person = new Person(firsName, lastName, cnp);

					idForCustomer = setId(cnp);
					person.setId(idForCustomer);
					cont.setId(id);

					manageBanck.addHolder(person, cont);

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

				} else
					JOptionPane.showMessageDialog(null, "Selecteaza contul de la care vrei sa stergi observatorul");

			}
		});
		buttonAddHolder.setBounds(35, 452, 125, 23);
		frame.getContentPane().add(buttonAddHolder);

		JButton buttonDeleteHolder = new JButton("Delete Holder");
		buttonDeleteHolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String firsName = "", lastName = "";
				int cnp = 0;
				Person person = null;
				int id;
				int selRow1 = tableOfCustomers.getSelectedRow();
				if (selRow1 != -1) {
					id = Integer.parseInt((String) (tableOfCustomers.getModel()).getValueAt(selRow1, 1));
					firsName = (String) (tableOfCustomers.getModel()).getValueAt(selRow1, 3);
					lastName = (String) (tableOfCustomers.getModel()).getValueAt(selRow1, 2);
					cnp = Integer.parseInt((String) (tableOfCustomers.getModel()).getValueAt(selRow1, 0));
					person = new Person(firsName, lastName, cnp);
					person.setId(id);
				} else
					JOptionPane.showMessageDialog(null, "Selecteaza contul de la care vrei sa stergi observatorul");

				String type;
				float money;
				String id1;
				Account cont = null;
				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1 && person != null) {

					id1 = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}
					cont.setId(id1);
					manageBanck.removeHolder(person, cont);

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);

				} else
					JOptionPane.showMessageDialog(null, "Selecteaza contul de la care vrei sa stergi observatorul");

			}
		});
		buttonDeleteHolder.setBounds(35, 486, 125, 23);
		frame.getContentPane().add(buttonDeleteHolder);

		txtId = new JTextField();
		txtId.setText("ID");
		txtId.setBounds(295, 472, 86, 20);
		frame.getContentPane().add(txtId);
		txtId.setColumns(10);

		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(188, 475, 46, 14);
		frame.getContentPane().add(lblId);

		JButton btnRate = new JButton("Rate");
		btnRate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id;
				String type;
				float money;
				Account cont = null;

				int selRow = tableOfAccount.getSelectedRow();
				if (selRow != -1) {

					id = (String) (tableOfAccount.getModel()).getValueAt(selRow, 0);
					type = (String) (tableOfAccount.getModel()).getValueAt(selRow, 1);
					money = Float.parseFloat((String) tableOfAccount.getModel().getValueAt(selRow, 2));

					if (type.equals("Spending")) {
						cont = new SpendingAccount(type, money);
					} else {
						cont = new SavingAccount(type, money);
					}
					cont.setId(id);

					if (cont instanceof SavingAccount) {
						manageBanck.addRate(cont);
					}
					// manageBanck.removeAccount(cont);

					modelForCustomers = setTable();
					tableOfCustomers.setModel(modelForCustomers);

					modelForAccounts = setTableForAccounts();
					tableOfAccount.setModel(modelForAccounts);
				} else
					JOptionPane.showMessageDialog(null, "Alege contul");

			}
		});
		btnRate.setBounds(929, 265, 89, 23);
		frame.getContentPane().add(btnRate);

	}

	public int setId(int cnp) {
		Iterator it = manageBanck.getManageAccount().entrySet().iterator();

		int nr = 0;
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if (entry.getKey().getcnp() == cnp)
				nr++;
		}

		if (nr == 0)
			idForCustomer = 1;
		else if (nr == 1)
			idForCustomer = 2;
		return idForCustomer;
	}

	public String setIdForAcount(Account account) {

		if (account instanceof SavingAccount) {
			idForAccount = new String("sa" + idForCustomer);
		} else {
			idForAccount = new String("sp" + idForCustomer);
		}
		return idForAccount;
	}

	public void serialization() {
		FileOutputStream fileOut;
	
		HashMap<Person, Account> manageAccount = new HashMap<Person, Account>();
		manageAccount=	manageBanck.getManageAccount();
		try {
			fileOut = new FileOutputStream("BanckStore");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(manageAccount);
			out.close();
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public DefaultTableModel setTable() {

		Vector<String> row;

		if (modelForCustomers.getRowCount() > 0) {
			for (int i = modelForCustomers.getRowCount() - 1; i > -1; i--) {
				modelForCustomers.removeRow(i);
			}
		}

		Iterator it = manageBanck.getManageAccount().entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			row = new Vector<>();
			row.add(Integer.toString(entry.getKey().getcnp()));
			row.add(Integer.toString(entry.getKey().getId()));
			row.add(entry.getKey().getFirstName());
			row.add(entry.getKey().getLastName());
			modelForCustomers.addRow(row);
		}
		return modelForCustomers;

	}

	public DefaultTableModel setTableForAccounts() {
		Vector<String> row;

		if (modelForAccounts.getRowCount() > 0) {
			for (int i = modelForAccounts.getRowCount() - 1; i > -1; i--) {
				modelForAccounts.removeRow(i);
			}
		}

		Iterator it = manageBanck.getManageAccount().entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			row = new Vector<>();
			row.add(entry.getValue().getId());
			row.add(entry.getValue().getType());
			row.add(entry.getValue().getMoney() + "");
			modelForAccounts.addRow(row);
		}
		return modelForAccounts;

	}

	void sth() {
		Person p1 = new Person("Adela", "Aciu", 122413546);
		p1.setId(1);
		Account acc = new SavingAccount("SavingAccount", 250);
		acc.setId(" sa" + 1);
		manageBanck.addAcount(p1, acc);
	}

	public DefaultListModel<String> addToList(ArrayList<banck.Observer> persons) {

		for (Observer o : persons) {
			modelForJList.addElement(((Person) o).toString());
		}
		return modelForJList;
	}

	void displayObservants(String id) {
		JFrame frameForPerson = new JFrame();
		frameForPerson.setBounds(400, 200, 190, 200);
		modelForJList = new DefaultListModel();

		Vector<String> row;
		ArrayList<banck.Observer> persons;
		Iterator it = manageBanck.getManageAccount().entrySet().iterator();
		Map.Entry<Person, Account> entry;
		while (it.hasNext()) {
			entry = (Map.Entry) it.next();
			if (entry.getValue().getId().equals(id)) {
				persons = entry.getValue().getObservers();
				modelForJList = addToList(persons);

			}
		}
		JListPersons = new JList(modelForJList);
		JListPersons.setBounds(0, 0, 190, 200);
		JScrollPane scroolPaneForList = new JScrollPane(JListPersons);
		scroolPaneForList.setBounds(0, 0, 190, 200);
		scroolPaneForList.setVisible(true);
		frameForPerson.getContentPane().add(scroolPaneForList);
		frameForPerson.setTitle("Acces");
		frameForPerson.setVisible(true);
	}
}
