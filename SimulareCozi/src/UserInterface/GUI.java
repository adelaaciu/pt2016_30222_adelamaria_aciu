package UserInterface;

import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import client.Customer;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.awt.event.ActionEvent;
import manage.*;
import javax.swing.JTextArea;

public class GUI {
	int max = -1;
	private JFrame frame;
	private JTextField textFMinAstept;
	private JTextField textFMaxAstept;
	private JTextField textFieldTimpAsteptMax;
	private JTextField textFieldTimpAsteptMin;
	private JTextField textFieldTimpSim;
	private JTextField textFieldNoCozi;
	private JLabel lblTestare;
	private CustomerPanel cp;
	private JPanel panelC1;
	private int pozX = 0;
	private int pozY = 20;
	private int timeMax;
	private int timeMin;
	private int nrCozi;
	private int timpClMin;
	private int timpClMax, noMaxPers;
	private JTextField textFieldNoMaxCl;
	/*
	 * public List<Customer> customerList1; public List<Customer> customerList2;
	 * public List<Customer> customerList3; public List<Customer> customerList4;
	 */
	private JLabel label_3;
	private JLabel label_2;
	private JLabel label_1;
	private JLabel label;
	private int timpSim;
	private DefaultListModel<String> modelForJList;
	public int contor;
	private JList listEvolutie;
	private JPanel panel;
	public ManageCheckout manageCheckout;
	Calendar today;
	private List<CustomerPanel> panelList;
	public ManageCustomers manageCustomers;
	private JLabel lblDuratamediecasa;
	private JLabel lblServire1;
	private JLabel lblDuratamedie2;
	private JLabel lblServire2;
	private JLabel lblDuratamedie3;
	private JLabel lblServire3;
	private JLabel lblDuratamedie4;
	private JLabel lblServire4;
	private JLabel lblOravf;
	private JLabel lblCoziGoalee;

	// public static BlockingQueue<Customer> customersList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		today = new GregorianCalendar();
		/*
		 * customerList1 = new ArrayList<Customer>(30); customerList2 = new
		 * ArrayList<Customer>(30); customerList3 = new ArrayList<Customer>(30);
		 * customerList4 = new ArrayList<Customer>(30);
		 */

		panelList = new ArrayList<CustomerPanel>();
		frame = new JFrame();
		frame.setBounds(100, 100, 924, 595);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panelImput = new JPanel();
		panelImput.setBounds(0, 0, 749, 89);
		frame.getContentPane().add(panelImput);
		panelImput.setLayout(null);

		JLabel lblTimpAsteptarePentru = new JLabel("Timp asteptare pentru servire : ");
		lblTimpAsteptarePentru.setBounds(10, 11, 180, 14);
		panelImput.add(lblTimpAsteptarePentru);

		textFMinAstept = new JTextField();
		textFMinAstept.setBounds(202, 5, 58, 20);
		panelImput.add(textFMinAstept);
		textFMinAstept.setColumns(10);

		textFMaxAstept = new JTextField();
		textFMaxAstept.setColumns(10);
		textFMaxAstept.setBounds(270, 5, 58, 20);
		panelImput.add(textFMaxAstept);

		JLabel lblTimpSimulare = new JLabel("Timp simulare : ");
		lblTimpSimulare.setBounds(10, 36, 74, 14);
		panelImput.add(lblTimpSimulare);

		JLabel lblTimpAsteptareIntre = new JLabel("Timp asteptare intre clienti");
		lblTimpAsteptareIntre.setBounds(372, 11, 171, 14);
		panelImput.add(lblTimpAsteptareIntre);

		textFieldTimpAsteptMax = new JTextField();
		textFieldTimpAsteptMax.setColumns(10);
		textFieldTimpAsteptMax.setBounds(640, 8, 58, 20);
		panelImput.add(textFieldTimpAsteptMax);

		textFieldTimpAsteptMin = new JTextField();
		textFieldTimpAsteptMin.setColumns(10);
		textFieldTimpAsteptMin.setBounds(553, 8, 58, 20);
		panelImput.add(textFieldTimpAsteptMin);

		textFieldTimpSim = new JTextField();
		textFieldTimpSim.setBounds(93, 33, 51, 20);
		panelImput.add(textFieldTimpSim);
		textFieldTimpSim.setColumns(10);

		JLabel lblNumarCozi = new JLabel("Numar cozi");
		lblNumarCozi.setBounds(182, 36, 91, 14);
		panelImput.add(lblNumarCozi);

		textFieldNoCozi = new JTextField();
		textFieldNoCozi.setBounds(250, 33, 58, 20);
		panelImput.add(textFieldNoCozi);
		textFieldNoCozi.setColumns(10);

		JButton btnStartSimulare = new JButton("Start simulare");
		btnStartSimulare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				timeMax = Integer.parseInt(textFieldTimpAsteptMax.getText());
				timeMin = Integer.parseInt(textFieldTimpAsteptMin.getText());
				nrCozi = Integer.parseInt(textFieldNoCozi.getText());
				timpClMin = Integer.parseInt(textFieldTimpAsteptMin.getText());
				timpClMax = Integer.parseInt(textFieldTimpAsteptMax.getText());
				noMaxPers = Integer.parseInt(textFieldNoMaxCl.getText());
				timpSim = Integer.parseInt(textFieldTimpSim.getText());
				Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
				worker.execute();
				try {
					// System.out.println(" Inainte de hmmm ");
					manageCustomers = new ManageCustomers(timeMax, timeMin, nrCozi, timpClMin, timpClMax, noMaxPers);
					hmm();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println(" Dupa hmmm ");
			}
		});
		btnStartSimulare.setBounds(469, 42, 142, 36);
		panelImput.add(btnStartSimulare);

		JLabel lblNumarMaximClienti = new JLabel("Numar maxim clienti");
		lblNumarMaximClienti.setBounds(10, 64, 134, 14);
		panelImput.add(lblNumarMaximClienti);

		textFieldNoMaxCl = new JTextField();
		textFieldNoMaxCl.setBounds(172, 61, 86, 20);
		panelImput.add(textFieldNoMaxCl);
		textFieldNoMaxCl.setColumns(10);

		panelC1 = new JPanel();
		panelC1.setBounds(10, 100, 407, 123);
		frame.getContentPane().add(panelC1);
		panelC1.setLayout(null);
		panelC1.setBorder(BorderFactory.createLineBorder(Color.BLUE));

		lblTestare = new JLabel("0");
		lblTestare.setBounds(0, 0, 407, 112);
		panelC1.add(lblTestare);

		JPanel panelC2 = new JPanel();
		panelC2.setBounds(10, 326, 407, 66);
		frame.getContentPane().add(panelC2);
		panelC2.setLayout(null);
		panelC2.setBorder(BorderFactory.createLineBorder(Color.GREEN));

		JButton btnCasa_1 = new JButton("Casa2");
		btnCasa_1.setEnabled(false);
		btnCasa_1.setBackground(new Color(85, 107, 47));
		btnCasa_1.setBounds(211, 11, 172, 44);
		panelC2.add(btnCasa_1);

		label_1 = new JLabel("0");
		label_1.setBounds(59, 26, 46, 14);
		panelC2.add(label_1);

		JPanel panelC3 = new JPanel();
		panelC3.setBounds(10, 403, 407, 66);
		frame.getContentPane().add(panelC3);
		panelC3.setLayout(null);
		panelC3.setBorder(BorderFactory.createLineBorder(Color.GREEN));

		JButton btnCasa_2 = new JButton("Casa3");
		btnCasa_2.setEnabled(false);
		btnCasa_2.setBackground(new Color(85, 107, 47));
		btnCasa_2.setBounds(211, 11, 172, 44);
		panelC3.add(btnCasa_2);

		label_2 = new JLabel("0");
		label_2.setBounds(57, 26, 46, 14);
		panelC3.add(label_2);

		JPanel panelC4 = new JPanel();
		panelC4.setBounds(10, 480, 407, 66);
		frame.getContentPane().add(panelC4);
		panelC4.setLayout(null);
		panelC4.setBorder(BorderFactory.createLineBorder(Color.GREEN));

		JButton btnCasa_3 = new JButton("Casa4");
		btnCasa_3.setEnabled(false);
		btnCasa_3.setBackground(new Color(85, 107, 47));
		btnCasa_3.setBounds(213, 11, 172, 44);
		panelC4.add(btnCasa_3);

		label_3 = new JLabel("0");
		label_3.setBounds(58, 26, 46, 14);
		panelC4.add(label_3);

		panel = new JPanel();
		panel.setBounds(561, 134, 337, 412);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		modelForJList = new DefaultListModel();
		listEvolutie = new JList(modelForJList);
		listEvolutie.setBounds(10, 11, 317, 390);
		JScrollPane scroolPaneForList = new JScrollPane(listEvolutie);
		scroolPaneForList.setBounds(10, 11, 317, 390);
		scroolPaneForList.setVisible(true);

		panel.add(scroolPaneForList);

		JLabel lblEvolutie = new JLabel(" Evolutie");
		lblEvolutie.setBounds(702, 100, 70, 23);
		frame.getContentPane().add(lblEvolutie);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(BorderFactory.createLineBorder(Color.GREEN));
		panel_1.setBounds(10, 234, 407, 66);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JButton btnCasa = new JButton("Casa1");
		btnCasa.setEnabled(false);
		btnCasa.setBackground(new Color(85, 107, 47));
		btnCasa.setBounds(211, 11, 172, 44);
		panel_1.add(btnCasa);

		label = new JLabel("0");
		label.setBounds(65, 26, 46, 14);
		panel_1.add(label);

		lblDuratamediecasa = new JLabel("0");
		lblDuratamediecasa.setBounds(439, 256, 38, 23);
		frame.getContentPane().add(lblDuratamediecasa);

		lblServire1 = new JLabel("0");
		lblServire1.setBounds(487, 256, 39, 23);
		frame.getContentPane().add(lblServire1);

		lblDuratamedie2 = new JLabel("0");
		lblDuratamedie2.setBounds(439, 350, 38, 23);
		frame.getContentPane().add(lblDuratamedie2);

		lblServire2 = new JLabel("0");
		lblServire2.setBounds(487, 350, 39, 23);
		frame.getContentPane().add(lblServire2);

		lblDuratamedie3 = new JLabel("0");
		lblDuratamedie3.setBounds(439, 422, 38, 23);
		frame.getContentPane().add(lblDuratamedie3);

		lblServire3 = new JLabel("0");
		lblServire3.setBounds(487, 422, 39, 23);
		frame.getContentPane().add(lblServire3);

		lblDuratamedie4 = new JLabel("0");
		lblDuratamedie4.setBounds(439, 501, 38, 23);
		frame.getContentPane().add(lblDuratamedie4);

		lblServire4 = new JLabel("0");
		lblServire4.setBounds(487, 501, 39, 23);
		frame.getContentPane().add(lblServire4);

		JLabel lblOraDeVarf = new JLabel("Ora de varf");
		lblOraDeVarf.setBounds(439, 104, 87, 14);
		frame.getContentPane().add(lblOraDeVarf);

		lblOravf = new JLabel("OraVf");
		lblOravf.setBounds(521, 109, 46, 14);
		frame.getContentPane().add(lblOravf);

		JLabel lblCoziGoale = new JLabel("Cozi goale");
		lblCoziGoale.setBounds(439, 163, 87, 14);
		frame.getContentPane().add(lblCoziGoale);

		lblCoziGoalee = new JLabel("0");
		lblCoziGoalee.setBounds(449, 188, 46, 35);
		frame.getContentPane().add(lblCoziGoalee);

	}

	/**
	 * Generare clienti vizual
	 */
	SwingWorker<Boolean, Integer> worker = new SwingWorker<Boolean, Integer>() {
		@Override
		protected Boolean doInBackground() throws Exception {
			// Simulate doing something useful.
			for (int i = 1; i <= noMaxPers; i++) {
				Thread.sleep(100);

				publish(i);

			}

			return true;
		}

		// Can safely update the GUI from this method.
		protected void done() {

			boolean status;
			try {
				// Retrieve the return value of doInBackground.
				status = get();
			} catch (InterruptedException e) {
				// This is thrown if the thread's interrupted.
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		// Can safely update the GUI from this method.
		protected void process(List<Integer> chunks) {

			int mostRecentValue = chunks.get(chunks.size() - 1);

			if (pozX >= 380) {
				pozY = pozY + 22;
				pozX = 15;
			}

			else
				pozX = pozX + 23;

			cp = new CustomerPanel(pozX, pozY);
			panelC1.add(cp);
			panelList.add(cp);
			lblTestare.setText(Integer.toString(mostRecentValue));

		}

	};
	boolean once;
	int index;
	int pozz;
	int sec;
	int val;
	String maximH;
	int maxSize;

	public void hmm() throws InterruptedException {

		if (manageCustomers.customersList.isEmpty() == false) {
			manageCustomers.removeClientFromWaiting();
		}
		if (once == false) {
			manageCheckout = new ManageCheckout(nrCozi);
			once = true;
		}

		manageCheckout.updateList(ManageCustomers.personPay);

		System.out.println("Aici");
		sec = today.get(Calendar.SECOND);
		timpSim = sec + timpSim;
		System.out.println(sec + " " + timpSim);

		Thread th2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				worker1.execute();
			}
		});
		Thread th3 = new Thread(new Runnable() {

			@Override
			public void run() { // TODO Auto-generated method stub
				// manageCheckout.updateList(manageCustomers.personPay);
				worker2.execute();
			}
		});
		Thread th4 = new Thread(new Runnable() {

			@Override
			public void run() { // TODO Auto-generated method stub

				worker3.execute();
			}
		});

		Thread th5 = new Thread(new Runnable() {

			@Override
			public void run() { // TODO Auto-generated method stub
				worker4.execute();
			}
		});

		th2.start();
		if (nrCozi >= 2)
			th3.start();
		if (nrCozi >= 3)
			th4.start();
		if (nrCozi == 4)
			th5.start();

		th2.join();
		if (nrCozi >= 2)
			th3.join();
		if (nrCozi >= 3)
			th4.join();
		if (nrCozi == 4)
			th5.join();

	}

	SwingWorker<Boolean, Integer> worker1 = new SwingWorker<Boolean, Integer>() {
		@Override
		protected Boolean doInBackground() throws Exception {
			// Simulate doing something useful.
			sec = today.get(Calendar.SECOND);
			while (sec < timpSim) {
				// customerList1 = manageCheckout.getCustomerList1();
				if (manageCheckout.getCustomerList1().size() != 0) {
					Thread.sleep(manageCheckout.getCustomerList1().get(0).getServingTime());
					publish(manageCheckout.getCustomerList1().size());
				}
				contor++;
			}
			return true;
		}

		// Can safely update the GUI from this method.
		protected void done() {

			boolean status;
			try {
				// Retrieve the return value of doInBackground.
				status = get();
			} catch (InterruptedException e) {
				// This is thrown if the thread's interrupted.
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		// Can safely update the GUI from this method.
		protected void process(List<Integer> chunks) {
			System.out.println("Ajunge");
			int mostRecentValue = chunks.get(chunks.size() - 1);
			label.setText(Integer.toString(mostRecentValue));

			if (manageCheckout.getCustomerList1().size() != 0) {

				modelForJList.addElement(manageCheckout.getCustomerList1().get(0).toString() + " Casa 1 \n");
				listEvolutie.setModel(modelForJList);
				for (CustomerPanel c : panelList) {
					if (c.getCoordX() == manageCheckout.getCustomerList1().get(0).getPositonX()
							&& c.getCoordY() == manageCheckout.getCustomerList1().get(0).getPositionY()) {
						c.hide();
						val = (Integer.parseInt(lblTestare.getText()) - 1);
						lblTestare.setText(val + "");
						panelList.remove(c);
						break;
					}
				}

			}

			lblDuratamediecasa.setText(manageCheckout.timpMediuAsteptareCoada(manageCheckout.getCustomerList1()) + "");
			lblServire1.setText(manageCheckout.timpMediuServireCoada(manageCheckout.getCustomerList1()) + "");
			if (manageCheckout.ClientiMax() > maxSize) {
				maxSize = manageCheckout.ClientiMax();
				int hour = today.get(Calendar.HOUR);
				int min = today.get(Calendar.MINUTE);
				lblOravf.setText(hour + " : " + min);
			}
			manageCheckout.removeFromQueue(manageCheckout.getCustomerList1());

			if (manageCheckout.getCustomerList1().size() == 0 && manageCheckout.getCustomerList2().size() == 0
					&& manageCheckout.getCustomerList3().size() == 0 && manageCheckout.getCustomerList4().size() == 0) {
				float s = today.get(Calendar.SECOND) / 4;
				lblCoziGoalee.setText(s + "");
			}
			System.out.println("Intra in remove si update");
			manageCustomers.removeClientFromWaiting();
			manageCheckout.updateList(ManageCustomers.personPay);

		}

	};

	SwingWorker<Boolean, Integer> worker2 = new SwingWorker<Boolean, Integer>() {
		@Override
		protected Boolean doInBackground() throws Exception {
			// Simulate doing something useful.
			// customerList2 = manageCheckout.getCustomerList2();
			sec = today.get(Calendar.SECOND);
			while (sec < timpSim) {
				if (manageCheckout.getCustomerList2().size() != 0) {
					Thread.sleep(manageCheckout.getCustomerList2().get(0).getServingTime());
					publish(manageCheckout.getCustomerList2().size());
				}
				contor++;
			}
			return true;
		}

		// Can safely update the GUI from this method.
		protected void done() {

			boolean status;
			try {
				// Retrieve the return value of doInBackground.
				status = get();
			} catch (InterruptedException e) {
				// This is thrown if the thread's interrupted.
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		// Can safely update the GUI from this method.
		protected void process(List<Integer> chunks) {
			System.out.println("Ajunge2");
			int mostRecentValue = chunks.get(chunks.size() - 1);

			label_1.setText(Integer.toString(mostRecentValue));
			if (manageCheckout.getCustomerList2().size() != 0) {

				modelForJList.addElement(manageCheckout.getCustomerList2().get(0).toString() + " Casa 2 \n");
				listEvolutie.setModel(modelForJList);
				for (CustomerPanel c : panelList) {
					if (c.getCoordX() == manageCheckout.getCustomerList2().get(0).getPositonX()
							&& c.getCoordY() == manageCheckout.getCustomerList2().get(0).getPositionY()) {
						c.hide();
						val = (Integer.parseInt(lblTestare.getText()) - 1);
						lblTestare.setText(val + "");
						panelList.remove(c);
						break;
					}
				}

			}

			lblDuratamedie2.setText(manageCheckout.timpMediuAsteptareCoada(manageCheckout.getCustomerList2()) + "");
			lblServire2.setText(manageCheckout.timpMediuServireCoada(manageCheckout.getCustomerList2()) + "");
			if (manageCheckout.ClientiMax() > maxSize) {
				maxSize = manageCheckout.ClientiMax();
				int hour = today.get(Calendar.HOUR);
				int min = today.get(Calendar.MINUTE);
				lblOravf.setText(hour + " : " + min);
			}
			manageCheckout.removeFromQueue(manageCheckout.getCustomerList2());

			if (manageCheckout.getCustomerList1().size() == 0 && manageCheckout.getCustomerList2().size() == 0
					&& manageCheckout.getCustomerList3().size() == 0 && manageCheckout.getCustomerList4().size() == 0) {
				float s = (float) today.get(Calendar.SECOND) / 4;
				lblCoziGoalee.setText(s + "");
			}
			System.out.println("Intra in remove si update");
			manageCustomers.removeClientFromWaiting();
			manageCheckout.updateList(ManageCustomers.personPay);
		}

	};

	SwingWorker<Boolean, Integer> worker3 = new SwingWorker<Boolean, Integer>() {
		@Override
		protected Boolean doInBackground() throws Exception {
			// Simulate doing something useful.
			// customerList3 = manageCheckout.getCustomerList3();
			sec = today.get(Calendar.SECOND);
			while (sec < timpSim) {
				if (manageCheckout.getCustomerList3().size() != 0) {
					Thread.sleep(manageCheckout.getCustomerList3().get(0).getServingTime());
					publish(manageCheckout.getCustomerList3().size());
				}
				contor++;
			}
			return true;
		}

		// Can safely update the GUI from this method.
		protected void done() {

			boolean status;
			try {
				// Retrieve the return value of doInBackground.
				status = get();
			} catch (InterruptedException e) {
				// This is thrown if the thread's interrupted.
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		// Can safely update the GUI from this method.
		protected void process(List<Integer> chunks) {
			System.out.println("Ajunge3");
			int mostRecentValue = chunks.get(chunks.size() - 1);
			label_2.setText(Integer.toString(mostRecentValue));

			if (manageCheckout.getCustomerList3().size() != 0) {

				modelForJList.addElement(manageCheckout.getCustomerList3().get(0).toString() + " Casa 3 \n");
				listEvolutie.setModel(modelForJList);

				for (CustomerPanel c : panelList) {
					if (c.getCoordX() == manageCheckout.getCustomerList3().get(0).getPositonX()
							&& c.getCoordY() == manageCheckout.getCustomerList3().get(0).getPositionY()) {
						c.hide();
						val = (Integer.parseInt(lblTestare.getText()) - 1);
						lblTestare.setText(val + "");
						// panelList.remove(c);
						break;
					}
				}



			}

			lblDuratamedie3.setText(manageCheckout.timpMediuAsteptareCoada(manageCheckout.getCustomerList3()) + "");
			lblServire3.setText(manageCheckout.timpMediuServireCoada(manageCheckout.getCustomerList3()) + "");
			if (manageCheckout.ClientiMax() > maxSize) {
				maxSize = manageCheckout.ClientiMax();
				int hour = today.get(Calendar.HOUR);
				int min = today.get(Calendar.MINUTE);
				lblOravf.setText(hour + " : " + min);
			}
			manageCheckout.removeFromQueue(manageCheckout.getCustomerList3());

			if (manageCheckout.getCustomerList1().size() == 0 && manageCheckout.getCustomerList2().size() == 0
					&& manageCheckout.getCustomerList3().size() == 0 && manageCheckout.getCustomerList4().size() == 0) {
				int s = today.get(Calendar.SECOND) / 4;
				lblCoziGoalee.setText(s + "");
			}
			System.out.println("Intra in remove si update");
			manageCustomers.removeClientFromWaiting();
			manageCheckout.updateList(ManageCustomers.personPay);
		}

	};

	SwingWorker<Boolean, Integer> worker4 = new SwingWorker<Boolean, Integer>() {
		@Override
		protected Boolean doInBackground() throws Exception {
			// Simulate doing something useful.
			// customerList4 = manageCheckout.getCustomerList4();
			sec = today.get(Calendar.SECOND);
			while (sec < timpSim) {

				if (manageCheckout.getCustomerList4().size() != 0) {
					Thread.sleep(manageCheckout.getCustomerList4().get(0).getServingTime());
					publish(manageCheckout.getCustomerList4().size());

				}
				contor++;
			}
			return true;
		}

		// Can safely update the GUI from this method.
		protected void done() {

			boolean status;
			try {
				// Retrieve the return value of doInBackground.
				status = get();
			} catch (InterruptedException e) {
				// This is thrown if the thread's interrupted.
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		// Can safely update the GUI from this method.
		protected void process(List<Integer> chunks) {
			System.out.println("Ajunge4");
			int mostRecentValue = chunks.get(chunks.size() - 1);
			if (manageCustomers.customersList.size() != 0) {
				System.out.println("Intra in remove si update");
				manageCustomers.removeClientFromWaiting();
				manageCheckout.updateList(ManageCustomers.personPay);

			}
			label_3.setText(Integer.toString(mostRecentValue));

			if (manageCheckout.getCustomerList4().size() != 0) {
				modelForJList.addElement(manageCheckout.getCustomerList4().get(0).toString() + " Casa 4 \n");
				listEvolutie.setModel(modelForJList);
				for (CustomerPanel c : panelList) {
					if (c.getCoordX() == manageCheckout.getCustomerList4().get(0).getPositonX()
							&& c.getCoordY() == manageCheckout.getCustomerList4().get(0).getPositionY()) {

						c.hide();
						val = (Integer.parseInt(lblTestare.getText()) - 1);
						lblTestare.setText(val + "");
						// panelList.remove(c);
						break;
					}
				}

			}
			lblDuratamedie4.setText(manageCheckout.timpMediuAsteptareCoada(manageCheckout.getCustomerList4()) + "");
			lblServire4.setText(manageCheckout.timpMediuServireCoada(manageCheckout.getCustomerList4()) + "");

			if (manageCheckout.ClientiMax() > maxSize) {
				maxSize = manageCheckout.ClientiMax();
				int hour = today.get(Calendar.HOUR);
				int min = today.get(Calendar.MINUTE);
				lblOravf.setText(hour + " : " + min);
			}
			manageCheckout.removeFromQueue(manageCheckout.getCustomerList4());

			if (manageCheckout.getCustomerList1().size() == 0 && manageCheckout.getCustomerList2().size() == 0
					&& manageCheckout.getCustomerList3().size() == 0 && manageCheckout.getCustomerList4().size() == 0) {
				float s = (float) today.get(Calendar.SECOND) / 4;
				lblCoziGoalee.setText(s + "");
			}
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// label_3.setText(Integer.toString(mostRecentValue + 1));
		}

	};
}
