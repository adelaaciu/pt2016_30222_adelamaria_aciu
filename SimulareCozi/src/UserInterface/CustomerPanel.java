package UserInterface;

import java.awt.FlowLayout;
import java.awt.*;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.concurrent.Future;

public class CustomerPanel extends JPanel {

	private int coordX, coordY;

	public CustomerPanel(int x, int y) {

		this.coordX = x;
		this.coordY = y;
		this.setBounds(x, y, 22, 22);
		this.setLayout(new FlowLayout());
		this.setVisible(true);
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.drawOval(0, 0, 20, 20);
		g.fillOval(0, 0, 20, 20);
		this.repaint();
	}

	public int getCoordX() {
		return coordX;
	}

	public int getCoordY() {
		return coordY;
	}
	


}
