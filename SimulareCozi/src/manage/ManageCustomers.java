package manage;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import client.*;

public class ManageCustomers {

	public static List<Customer> personPay;
	public static BlockingQueue<Customer> customersList;
	private int timeMax;
	private int timeMin;
	private int nrCozi;
	private int timpClMin;
	private int timpClMax, noMaxPers;

	public ManageCustomers(int timeMax, int timeMin, int maxCozi, int timpClMin, int timpClMax, int noMaxPers)
			throws InterruptedException {
		this.timeMax = timeMax;
		personPay = new ArrayList<Customer>();
		this.timeMin = timeMin;
		nrCozi = maxCozi;
		this.timpClMin = timpClMin;
		this.timpClMax = timpClMax;
		this.noMaxPers = noMaxPers;
		customersList = new ArrayBlockingQueue(noMaxPers);
		generateCustomers();
	}

	public void generateCustomers() throws InterruptedException {
		Customer c;

		int i, p, timeUntillPay;
		int waitingTime;
		i = 0;
		int pozX = 0;
		int pozY = 20;

		while (i < noMaxPers) {
			if (pozX >= 380) {
				pozY = pozY + 22;
				pozX = 15;
			}

			else
				pozX = pozX + 23;
			Random rand = new Random();
			Random rnd = new Random();
			p = rand.nextInt(timeMax - timeMin) + timeMin;

			timeUntillPay = rnd.nextInt(timpClMax - timpClMin) - timpClMin; /// **************************************
			/// modifica in functie de
			/// timpul de generare

			waitingTime = p * 1000; // 1 000 = 1s
			timeUntillPay = timeUntillPay * 1000;
			// pozitia se va seta ulterior in functie de casa de marcat la care
			// se
			// va sta
			c = new Customer(waitingTime, pozX, pozY, timeUntillPay);

			customersList.put(c);
			i++;
		}
	}

	public void removeClientFromWaiting() {
		int noOfPerson, i;
		Random rnd = new Random();
		noOfPerson = rnd.nextInt(10);
		System.out.println(noOfPerson);
		i = 0;
		if (customersList.size() != 0)
			while (i < noOfPerson && customersList.size() != 0) {
				// System.out.println(i);

				Customer c = customersList.remove();
				personPay.add(c);
				i++;

			}
		noMaxPers = noMaxPers - noOfPerson;

	}

}
