package manage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import client.Customer;

public class ManageCheckout {

	// private List<Customer> personPay;
	static List<Customer> customerList1;
	static List<Customer> customerList2;
	static List<Customer> customerList3;
	static List<Customer> customerList4;
	private int noOfCozi;

	public ManageCheckout(int noOfCozi)// ,
										// List<Customer>
										// customerList1,
	// List<Customer> customerList2, List<Customer> customerList3,
	// List<Customer> customerList4)
	{
		// this.personPay = personPay;
		this.noOfCozi = noOfCozi;
		this.customerList1 = new ArrayList<Customer>();
		this.customerList2 = new ArrayList<Customer>();
		this.customerList3 = new ArrayList<Customer>();
		this.customerList4 = new ArrayList<Customer>();
	}

	/**
	 * In functie de numarul de persoane care doresc sa placteasca, acestea sunt
	 * dispuse la anumite case de marcat pentru a avea un timp optim de serivr
	 * 
	 * @param personPay
	 *            Lista de clienti
	 */
	public void updateList(List<Customer> personPay) {
		// this.personPay = personPay;
		int i = 0;
		int time1 = 0;
		int time2 = 0;
		int time3 = 0;
		int time4 = 0;
		// System.out.println("Size : " + personPay.size());
		if (personPay.size() != 0)
			if (customerList1.size() == 0) {
				customerList1.add(personPay.remove(i));

			}
		if (personPay.size() != 0)
			if (noOfCozi >= 2 && customerList2.size() == 0) {
				customerList2.add(personPay.remove(i));

			}
		if (personPay.size() != 0)
			if (noOfCozi >= 3 && customerList3.size() == 0) {
				customerList3.add(personPay.remove(i));

			}
		if (personPay.size() != 0)
			if (noOfCozi == 4 && customerList4.size() == 0) {
				customerList4.add(personPay.remove(i));

			}
		// GRESEALA

		while (personPay.size() > 0) {
			System.out.println(personPay.size());
			time1 = totalTimeWaintin(customerList1);
			if (noOfCozi >= 2)
				time2 = totalTimeWaintin(customerList2);
			if (noOfCozi >= 3)
				time3 = totalTimeWaintin(customerList3);
			if (noOfCozi == 4)
				time4 = totalTimeWaintin(customerList4);

			if (noOfCozi == 1 && personPay.size() > 0) {
				customerList1.add(personPay.remove(i));
				// i++;
			}
			if (noOfCozi == 2) {
				if (time1 > time2) {
					customerList2.add(personPay.remove(i));
					// i++;
				} else {
					customerList1.add(personPay.remove(i));
					// i++;
				}
			}
			if (noOfCozi == 3 && personPay.size() > 0) {
				//////////////
				if (time3 <= time1 && time3 <= time2) {
					customerList3.add(personPay.remove(i));
					// i++;
				} else if (time2 <= time1 && time2 <= time3) {
					customerList3.add(personPay.remove(i));
					// i++;
				} else if (time1 <= time2 && time1 <= time3) {
					customerList3.add(personPay.remove(i));
					// i++;
				}
			}

			// ADAUGARE SI CUSTOMERLIST 4

			if (noOfCozi == 4 && personPay.size() > 0) {

				// *****************

				if (time4 <= time1 && time4 <= time2 && time4 <= time3) {
					customerList4.add(personPay.remove(i));
					// i++;
				} else if (time2 <= time1 && time2 <= time4 && time2 <= time3) {
					customerList2.add(personPay.remove(i));
					// i++;
				}

				else if (time1 <= time4 && time1 <= time2 && time1 <= time3) {
					customerList1.add(personPay.remove(i));
					// i++;
				}

				else if (time3 <= time1 && time3 <= time2 && time3 <= time4) {
					customerList3.add(personPay.remove(i));
					// i++;
				}

			}
		}
	}

	/**
	 * Timp total de asteptare la o coada
	 * 
	 * @param customerList
	 * @return
	 */
	public int totalTimeWaintin(List<Customer> customerList) {
		int nr = 0;
		if (customerList.size() != 0)
			for (Customer c : customerList) {
				nr += c.getServingTime() + c.getWaitingTime();
			}
		return nr;
	}

	public List<Customer> getCustomerList1() {
		return customerList1;
	}

	public List<Customer> getCustomerList2() {
		return customerList2;
	}

	public List<Customer> getCustomerList3() {
		return customerList3;
	}

	public List<Customer> getCustomerList4() {
		return customerList4;
	}

	public void removeFromQueue(List<Customer> customerList) {
		if (customerList.size() != 0)
			customerList.remove(0);
	}

	public float timpMediuAsteptareCoada(List<Customer> lista) {
		float timp = 0;
		for (Customer c : lista) {
			timp = timp + c.getServingTime() + c.getWaitingTime();
		}
		if (timp != 0) {
			timp = timp / lista.size();
			timp = timp / 1000;
		}
		return timp;
	}

	public float timpMediuServireCoada(List<Customer> lista) {
		float timp = 0;
		for (Customer c : lista) {
			timp = timp + c.getServingTime();
		}
		if (timp != 0) {
			timp = timp / lista.size();
			timp = timp / 1000;
		}
		return timp;
	}

	public int ClientiMax() {

		return customerList1.size() + customerList2.size() + customerList3.size() + customerList4.size();
	}

}
