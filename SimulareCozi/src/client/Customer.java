package client;

public class Customer {

	private int positonX;
	private int positionY;
	private int servingTime;
	private int wait;

	public Customer(int served, int x, int y,int timeUntillPay) {

		this.positonX = x;
		this.positionY = y;
		this.servingTime = served;
		this.wait=timeUntillPay;
	}

	public int getPositonX() {
		return positonX;
	}

	public void setPositonX(int positonX) {
		this.positonX = positonX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	public int getServingTime() {
		return servingTime;
	}

	public int getWaitingTime() {
		return wait;
	}
	@Override
	public String toString() {
		return "   Customer positonX=" + positonX + ", positionY=" + positionY + ", servingTime=" + servingTime
				+ ", wait=" + wait + " ";
	}


	
	

}
